<?php
/*
 * @return a unique key based on timestamp
 * */
function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function maturity_level($score)
{
    switch ($score) {
        case $score >= 0 && $score < 20:
            return ["level" => 1, "name" => "Discovery", "text" => "Operational Reporting", "result" => "Statistical or analytic tools have been purchased to meet an immediate need, but team lacks analytics expertise. Describes and summarizes your data."];
            break;
        case $score >= 20 && $score < 40:
            return ["level" => 2, "name" => "Foundation", "text" => "Advanced Reporting", "result" => "Full commitment to analytics for player performance, optimization and reaching team goals."];
            break;
        case $score >= 40 && $score < 60:
            return ["level" => 3, "name" => "Competitive", "text" => "Strategic Analytics", "result" => "Commitment to aligning analytics to other departments in order to reach organizational goals."];
            break;
        case $score >= 60 && $score < 80:
            return ["level" => 4, "name" => "Predictive", "text" => "Predictive Analytics", "result" => "Analytics yeild financial and performance gains across the team and departments. Focus is on forecating and probabilities. Dynamic future scenario modelling."];
            break;
        case $score >= 80 && $score <= 100:
            return ["level" => 5, "name" => "Innovative", "text" => "Prescriptive Analytics", "result" => "Machine intelligence. Transformational data. Data driven decision making. Focus is on prediction, optimization and innovation across the organization."];
            break;
    }
}


function isAdmin()
{
    return 1;
}

?>
