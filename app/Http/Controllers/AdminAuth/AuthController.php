<?php

namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        if (Auth::user()) {
            if (Auth::user()->type == isAdmin()) {
                return redirect('/admin');
            } else if (Auth::user()->type != isAdmin()) {
                return redirect('/');
            }
        }

        return view('admin.login');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::guard()->logout();

        $request->session()->invalidate();

        return redirect('/admin/login');
    }
}
