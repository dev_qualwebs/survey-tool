<?php

namespace App\Http\Controllers;

use App\Models\Answers;
use App\Models\Questions;
use App\Models\Challenges;
use App\Models\TeamProfile;
use App\Models\MaturityArea;
use App\Models\TeamChallenges;
use App\Models\AnalyticMethods;
use App\Models\BestPracticeArea;
use App\Models\TeamAnalyticMethods;
use App\Models\QuestionsAndAnswers;
use App\Models\WelcomeContent;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class AdminController extends Controller
{
    protected $user;
    protected $answers;

    protected $questions;
    protected $challenges;
    protected $teamProfile;
    protected $maturityArea;
    protected $teamChallenges;
    protected $analyticMethods;
    protected $bestPracticeArea;
    protected $teamAnalyticMethods;
    protected $questionsAndAnswers;
    protected $welcomeContent;

    public function __construct()
    {
        $this->middleware('admin');

        $this->user = new User;
        $this->answers = new Answers;
        $this->questions = new Questions;
        $this->challenges = new Challenges;
        $this->teamProfile = new TeamProfile;
        $this->maturityArea = new MaturityArea;
        $this->teamChallenges = new TeamChallenges;
        $this->analyticMethods = new AnalyticMethods;
        $this->bestPracticeArea = new BestPracticeArea;
        $this->teamAnalyticMethods = new TeamAnalyticMethods;
        $this->questionsAndAnswers = new QuestionsAndAnswers;
        $this->welcomeContent = new WelcomeContent;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\
     * returning view of question with data
     */
    public function loadQuestionsPage()
    {
        return view('admin.questions')->with($this->getQuestions());
    }

    public function loadBestPracticesPage()
    {
        return view('admin.best-practices')->with($this->getBestPracticeArea());
    }

    public function loadMaturityAreaPage()
    {
        return view('admin.maturity')->with($this->getMaturityAreas());
    }

    public function loadUsersPage()
    {
        return view('admin.users')->with($this->getAllUsers());
    }

    public function loadSingleQuestion(Request $request)
    {
        return view('admin.single-question')->with($this->getQuestionOptions($request));
    }

    public function loadBestPracticeArea()
    {
        return view('admin.add-best-practice-area')->with($this->getMaturityAreas());
    }

    /**
     * @return array
     * get all questions API
     */
    public function getQuestions()
    {
        $questions = $this->questions
            ->select('questions.id', 'questions.question', 'best_practice_area.name AS best_practice_area_name', 'maturity_area.name AS maturity_area_name')
            ->leftJoin('best_practice_area', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('maturity_area', 'maturity_area.id', '=', 'best_practice_area.maturity_area_id')
            ->get();
        return array('response' => $questions);
    }

    /**
     * @return array
     */
    public function getBestPracticeArea()
    {
        $response = $this->bestPracticeArea->with(['maturityArea' => function ($q) {
            $q->select('id', 'name');
        }])->select('id', 'name', 'maturity_area_id', 'recommendation')->get();
        return array("response" => $response);
    }

    /**
     * @return array
     * to show on list of maturity areas
     */
    public function getMaturityAreas()
    {
        $response = $this->maturityArea->get();
        return array('response' => $response);
    }

    public function getAllUsers()
    {
        $response = $this->user->get();
        return array('response' => $response);
    }

    // GET ALL QUESTION OPTIONS API
    function getQuestionOptions(Request $request)
    {
        $quid = $request->qid;

        $ques = $this->questions->where('id', '=', $quid)->get();

        $options = $this->answers->where('question_id', '=', $quid)->get();

        return array("response" => $options, "ques" => $ques->first());
    }

// GET ALL MATURITY AREAS
    function getMaturityArea()
    {
        $mAreas = $this->maturityArea->select('id', 'name')->get();
        return array("mareas" => $mAreas);
    }

// GET ALL QUESTION OPTIONS API
    function getBPArea(Request $request)
    {
        $mid = $request->mid;
        $bpAreas = $this->bestPracticeArea
            ->select('id', 'name', 'maturity_area_id')
            ->where('maturity_area_id', '=', $mid)
            ->get();
        return response()->json(["response" => $bpAreas], 200);
    }

// LOAD ADD QUESTION VIEW
    function addQuestion(Request $request)
    {
        return view('admin.add-question')->with($this->getMaturityArea());
    }

// Add QUESTION TO DB
    function postAddQuestion(Request $request)
    {
        $question = $this->questions;
        $question->best_practice_area_id = $request->input('bp_area');
        $question->question = $request->input('question');
        $question->save();

        $options = array(
            array('option' => 'A', 'text' => $request->input('option_a'), 'score' => 100, 'result_text' => $request->input('result_text_a'), 'question_id' => $question->id),
            array('option' => 'B', 'text' => $request->input('option_b'), 'score' => 75, 'result_text' => $request->input('result_text_b'), 'question_id' => $question->id),
            array('option' => 'C', 'text' => $request->input('option_c'), 'score' => 50, 'result_text' => $request->input('result_text_c'), 'question_id' => $question->id),
            array('option' => 'D', 'text' => $request->input('option_d'), 'score' => 25, 'result_text' => $request->input('result_text_d'), 'question_id' => $question->id),
            array('option' => 'E', 'text' => $request->input('option_e'), 'score' => 0, 'result_text' => $request->input('result_text_e'), 'question_id' => $question->id),
        );

        $this->answers->insert($options);

        return redirect('/admin/questions');
    }

    function editQuestion(Request $request)
    {
        $qid = $request->input('question_id');

        $question = $this->questions->find($qid);
        $question->question = $request->input('question');
        $question->save();

        $input = $request->input('answers');

        foreach ($input as $key => $val) {
            $this->answers->where('id', '=', $val['id'])->update([
                'text' => $val['text'],
                'result_text' => $val['result_text'],
            ]);
        }

        return redirect()->action('AdminController@loadSingleQuestion', ['qid' => $qid]);
    }

    function addPracticeArea(Request $request)
    {
        $best_practice_area = $this->bestPracticeArea;
        $best_practice_area->maturity_area_id = $request->input('maturity_area_id');
        $best_practice_area->name = $request->input('name');
        $best_practice_area->recommendation = $request->input('recommendation');
        $best_practice_area->save();
    }

    function loadAddMaturityArea()
    {
        return view('admin.add-maturity-area');
    }

    function addMaturityArea(Request $request)
    {
        $add_maturity_area = $this->maturityArea;
        $add_maturity_area->name = $request->input('name');
        $add_maturity_area->definition = $request->input('definition');
        $add_maturity_area->text = $request->input('text');
        $add_maturity_area->save();
    }

    function loadEditMaturityArea()
    {
        return view('admin.edit-maturity-area');
    }

    function singleMaturityArea(Request $request)
    {
        $maturity_id = $request->input('maturity_id');
        $maturity = $this->maturityArea->where('id', '=', $maturity_id)->get();
        return array("response" => $maturity->first());
    }

    function editSingleMaturity(Request $request)
    {
        $id = $request->input('id');
        $edit_maturity_area = $this->maturityArea->find($id);
        $edit_maturity_area->name = $request->input('name');
        $edit_maturity_area->definition = $request->input('definition');
        $edit_maturity_area->text = $request->input('text');
        $edit_maturity_area->save();
    }

    function loadEditBestPracticeArea()
    {
        return view('admin.edit-best-practice-area')->with($this->getMaturityAreas());
    }

    function singleBestPracticeArea(Request $request)
    {
        $practice_id = $request->input('practice_id');
        $practice = $this->bestPracticeArea->where('id', '=', $practice_id)->get();
        return array("response" => $practice->first());
    }

    function editSinglePracticeArea(Request $request)
    {
        $practice_id = $request->input('practice_id');
        $edit_practice_area = $this->bestPracticeArea->find($practice_id);
        $edit_practice_area->name = $request->input('name');
        $edit_practice_area->recommendation = $request->input('recommendation');
        $edit_practice_area->maturity_area_id = $request->input('maturity_area_id');
        $edit_practice_area->save();
    }

    function loadUserDetails()
    {
        return view('admin.user-detail');
    }

    function getUserProfile(Request $request)
    {
        $user_id = $request->input('user_id');
        $user_detail = $this->teamProfile
            ->select('team_profile.organization', 'team_profile.start_year', 'team_profile.sports', 'team_profile.competition_levels', 'team_profile.user_roles_company', 'users.name As user_name', 'users.email As user_email')
            ->leftJoin('users', 'users.user_id', '=', 'team_profile.user_id')->get();
        return array("response" => $user_detail->first());
    }

    public function userMaturityScore(Request $request)
    {
        $userId = $request->input('user_id');

        $reports = $this->teamProfile->ofUser($userId)->with('reportedAnswer.answerValue')->first();

        $reports = $reports->toArray();


        $score = 0;

        $noOfQuestions = count($reports['reported_answer']);

        foreach ($reports['reported_answer'] as $key => $val) {
            $score += $val['answer_value']['score'];
        }

        $avg = $score == 0 ? 0 : round($score / $noOfQuestions, 2);

        return response()->json(['score' => $avg, "data" => maturity_level($avg)]);
    }

    public function usersGraph()
    {
        $userCount = $this->user->count();

        $maturityAreaScore = $this->maturityArea
            ->select('maturity_area.name as maturity', 'maturity_area.definition', DB::raw('ROUND(AVG(answers.score), 2) as average'))
            ->leftJoin('best_practice_area', 'maturity_area.id', '=', 'best_practice_area.maturity_area_id')
            ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('question_and_answer', function ($q) {
                $q->on('questions.id', '=', 'question_and_answer.question_id');
            })
            ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
            ->groupBy('maturity_area.id')
            ->orderBy('maturity_area.name')
            ->get();


        $bestPracticeAreaScore = $this->bestPracticeArea
            ->select('best_practice_area.name as best_practice_area', 'maturity_area.name as maturity_area', DB::raw('ROUND(AVG(answers.score), 2) as average'))
            ->leftJoin('maturity_area', 'best_practice_area.maturity_area_id', '=', 'maturity_area.id')
            ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('question_and_answer', function ($q) {
                $q->on('questions.id', '=', 'question_and_answer.question_id');
            })
            ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
            ->groupBy('best_practice_area.id')
            ->orderBy('maturity_area.name')
            ->get();

        return response()->json(['userCount' => $userCount, 'maturity' => $maturityAreaScore, 'best_practice' => $bestPracticeAreaScore]);
    }

    public function loadWelcomeContent()
    {
        return view('admin.update-welcome-page');
    }

    public function getWelcomeContent()
    {
        $welcome_content = $this->welcomeContent->get();
        return response()->json(['response' => $welcome_content[0]]);
    }

    public function updateWelcomeContent(Request $request)
    {
        $update_welcome_content = $this->welcomeContent;
        $update_welcome_content = $this->welcomeContent->find(1);
        $update_welcome_content->heading_1 = $request->input('heading_1');
        $update_welcome_content->heading_2 = $request->input('heading_2');
        $update_welcome_content->bottom_content = $request->input('bottom_content');
        $update_welcome_content->image = $request->input('image');
        $update_welcome_content->home_page_link = $request->input('home_page_link');
        $update_welcome_content->sports_analytics_link = $request->input('sports_analytics_link');
        $update_welcome_content->attention_coaches_link = $request->input('attention_coaches_link');
        $update_welcome_content->elit_athlete_link = $request->input('elit_athlete_link');
        $update_welcome_content->contact_us_link = $request->input('contact_us_link');
        $update_welcome_content->save();
        return response()->json(['response' => 'Updated Successfully']);
    }

    public function uploadImage()
    {
        $file = Input::file('file');
        $size = $file->getClientSize();
        $filename = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        if ($ext != 'png' && $ext != 'PNG' && $ext != 'jpg' && $ext != 'JPG' && $ext != 'jpeg' && $ext != 'JPEG' && $ext != 'gif' && $ext != 'GIF' && $ext != 'bmp' && $ext != 'BMP') {
            return response()->json([
                'message' => "Not Allowed this ." . $ext . " extension only (png,jpg,jpeg,gif,bmp) allowed "
            ]);
        } else {
            $without_ext = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
            $filename = $without_ext . '_' . time() . '.' . $ext;
            $path = '/storage/app/public/attachment/' . $filename;
            $file->move(base_path('/storage/app/public/attachment'), $filename);
            return response()->json(['file_name' => $filename, 'ext' => $ext, 'path' => $path]);
        }
    }

}
