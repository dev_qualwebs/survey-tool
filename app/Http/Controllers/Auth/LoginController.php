<?php

namespace App\Http\Controllers\Auth;

use App\Models\UsersPositions;
use App\User;
use App\Http\Controllers\Controller;

use Exception;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if ($user->type == 1) {
            return redirect('/admin');
        }

        return redirect('/');
    }

    /**
     * Redirect the user to the LinkedIn authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToLinkedin()
    {
        return Socialite::driver('linkedin')->fields([
            'id', 'first-name', 'last-name', 'formatted-name',
            'email-address', 'headline', 'location', 'industry',
            'public-profile-url', 'picture-url', 'picture-urls::(original)', 'positions',
        ])->redirect();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     * Where to redirect users after linkedin login.
     */
    public function handleLinkedinCallback(Request $request)
    {
        if ($request->input('error') == 'user_cancelled_login') {
            dd('Authentication cancelled by the user');
        }

        try {
            $data = Socialite::driver('linkedin')->fields([
                'id', 'first-name', 'last-name', 'formatted-name',
                'email-address', 'headline', 'location', 'industry',
                'public-profile-url', 'picture-url', 'picture-urls::(original)', 'positions',
            ])->user();
        } catch (InvalidStateException $e) {
            $data = Socialite::driver('linkedin')->stateless()->fields([
                'id', 'first-name', 'last-name', 'formatted-name',
                'email-address', 'headline', 'location', 'industry',
                'public-profile-url', 'picture-url', 'picture-urls::(original)', 'positions',
            ])->user();
        }

        $users = new User;

        if (isset($data)) {
            $isEmailRegistered = !!count($users->where('email', '=', $data->email)->get());
        } else {
            dd('Couldn\'t authenticate the user');
        }

        if ($isEmailRegistered) {
            $id = $users->where('email', '=', $data->email)->first()->id;
            $userId = $users->where('email', '=', $data->email)->first()->user_id;

            $isLinkedinAvailable = !!count($users->where('email', '=', $data->email)->where('linkedin_id', '=', $data->id)->get());

            if (!$isLinkedinAvailable) {
                $users->where('id', '=', $id)->update(['linkedin_id' => $data->id]);
            }
            Auth::loginUsingId($id, true);
        } else {
            $userId = generateRandomString();
            $isUserIdAvailable = !!count($users->where('user_id', '=', $userId)->get());

            while ($isUserIdAvailable) {
                $userId = generateRandomString();
                $isUserIdAvailable = !!count($users->where('user_id', '=', $userId)->get());
            }

            $createdUser = $users->create([
                'user_id' => $userId,
                'name' => $data->name,
                'email' => $data->email,
                'password' => Hash::make($userId),
                'linkedin_id' => $data->id,
                'token' => $data->token
            ]);

            Auth::loginUsingId($createdUser->id, true);
        }

        (new UsersPositions)->where('user_id', '=', $userId)->delete();

        if ($data->user['positions']['_total']) {
            foreach ($data->user['positions']['values'] AS $key => $val) {
                $data = [
                    "user_id" => $userId,
                    "company_name" => $val['company']['name'],
                    "title" => $val['title'],
                    "isCurrent" => $val['isCurrent'] ? 1 : 0,
                    "start_month" => $val['startDate']['month'],
                    "start_year" => $val['startDate']['year'],
                ];

                (new UsersPositions)->create($data);
            }
        }

        return redirect()->route('welcome');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function handleLinkedinJsCallback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'emailAddress' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'positions' => 'required',
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        $email = $request->input('emailAddress');
        $name = $request->input('firstName') . ' ' . $request->input('lastName');
        $linkedIn = $request->input('id');
        $positions = $request->input('positions');

        $users = new User;

        $isEmailRegistered = !!count($users->where('email', '=', $email)->get());

        if ($isEmailRegistered) {
            $id = $users->where('email', '=', $email)->first()->id;
            $userId = $users->where('email', '=', $email)->first()->user_id;

            $isLinkedinAvailable = !!count($users->where('email', '=', $email)->where('linkedin_id', '=', $linkedIn)->get());

            if (!$isLinkedinAvailable) {
                $users->where('id', '=', $id)->update(['linkedin_id' => $linkedIn]);
            }
        } else {
            $userId = generateRandomString();
            $isUserIdAvailable = !!count($users->where('user_id', '=', $userId)->get());

            while ($isUserIdAvailable) {
                $userId = generateRandomString();
                $isUserIdAvailable = !!count($users->where('user_id', '=', $userId)->get());
            }

            $createdUser = $users->create([
                'user_id' => $userId,
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($userId),
                'linkedin_id' => $linkedIn
            ]);

            $id = $createdUser->id;
        }

        (new UsersPositions)->where('user_id', '=', $userId)->delete();

        if ($positions['_total']) {
            foreach ($positions['values'] AS $key => $val) {
                $data = [
                    "user_id" => $userId,
                    "company_name" => $val['company']['name'],
                    "title" => $val['title'],
                    "isCurrent" => $val['isCurrent'] ? 1 : 0,
                    "start_month" => $val['startDate']['month'],
                    "start_year" => $val['startDate']['year'],
                ];

                (new UsersPositions)->create($data);
            }
        }
        return response()->json(["response" => "success", "id" => $id]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function userLogin(Request $request)
    {
        Auth::loginUsingId($request->input('registeredId'), true);
        return redirect()->route('welcome');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Where to redirect users after Facebook login.
     *
     * @var string
     */

    public function handleFacebookCallback(Request $request)
    {
        if ($request->input('error') == 'access_denied' && $request->input('error_reason') == 'user_denied') {
            dd('Authentication cancelled by the user');
        }

        try {
            $data = Socialite::driver('facebook')->user();
        } catch (Exception $e) {
            if ($e instanceof InvalidStateException) {
                $data = Socialite::driver('facebook')->stateless()->user();
            }
        }

        $users = new User;

        $isEmailRegistered = !!count($users->where('email', '=', $data->email)->get());

        if ($isEmailRegistered) {
            $id = $users->where('email', '=', $data->email)->first()->id;
            $isFacebookAvailable = !!count($users->where('email', '=', $data->email)->where('facebook_id', '=', $data->id)->get());
            if (!$isFacebookAvailable) {
                $users->where('id', '=', $id)->update(['facebook_id' => $data->id]);
            }
            Auth::loginUsingId($id, true);
        } else {
            $userId = generateRandomString();
            $isUserIdAvailable = !!count($users->where('user_id', '=', $userId)->get());

            while ($isUserIdAvailable) {
                $userId = generateRandomString();
                $isUserIdAvailable = !!count($users->where('user_id', '=', $userId)->get());
            }

            $createdUser = $users->create([
                'user_id' => $userId,
                'name' => $data->name,
                'email' => $data->email,
                'password' => Hash::make($userId),
                'facebook_id' => $data->id,
            ]);

            Auth::loginUsingId($createdUser->id, true);
        }
        return redirect()->route('welcome');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
