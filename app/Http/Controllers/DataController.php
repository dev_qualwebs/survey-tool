<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use App\Models\Challenges;
use App\Models\TeamProfile;
use App\Models\MaturityArea;
use App\Models\AnalyticMethods;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DataController extends Controller
{
    protected $roles;
    protected $challenges;
    protected $teamProfile;
    protected $maturityArea;
    protected $analyticMethods;

    public function __construct()
    {
        $this->roles = new Roles;
        $this->challenges = new Challenges;
        $this->teamProfile = new TeamProfile;
        $this->maturityArea = new MaturityArea;
        $this->analyticMethods = new AnalyticMethods;
    }

    /*
     * function to get all available challenges
     * */
    public function getChallenges()
    {
        $response = $this->challenges->all();
        return response()->json(['response' => $response], 200);
    }

    /*
     * Function to get maturity
     * */
    public function getMaturityAreas()
    {
        $response = $this->maturityArea->all();
        return response()->json(['response' => $response], 200);
    }

    /*
     * Function to get questions based on maturity
     * */
    public function getMaturityQuestions(Request $request)
    {
        $maturityQuery = $this->maturityArea->with('bestPracticeArea.questions.options');

        if (Auth::check()) {
            if (Auth::user()->type == 0) {
                $userId = Auth::user()->user_id;
                try {
                    $teamProfileId = $this->teamProfile->ofUser($userId)->first()->id;

                    $maturityQuery = $this->maturityArea->with([
                        'bestPracticeArea.questions.selected' => function ($q) use ($teamProfileId) {
                            $q->where('team_profile_id', '=', $teamProfileId)->select('question_id', 'answer_id');
                        },
                        'bestPracticeArea.questions.options'
                    ]);
                } catch (Exception $e) {
                    //do nothing
                }
            }
        }

        if ($request->input('name')) {
            $maturityQuery->where('name', '=', $request->input('name'));
        }

        $response = $maturityQuery->get();

        return response()->json(['response' => $response], 200);
    }

    /*
     * Function to get analytics methods
     * */
    public function getAnalyticMethods()
    {
        $analyticsMethods = $this->analyticMethods->get();

        return response()->json(['response' => $analyticsMethods]);
    }

    /*
     * Function to get roles
     * */
    public function getRoles()
    {
        $roles = $this->roles->get();

        return response()->json(['response' => $roles]);
    }

}
