<?php

namespace App\Http\Controllers;

use App\Models\AnalyticMethods;
use App\Models\BestPracticeArea;
use App\Models\Challenges;
use App\Models\MaturityArea;
use App\Models\QuestionsAndAnswers;
use App\Models\Roles;
use App\Models\TeamAnalyticMethods;
use App\Models\TeamChallenges;
use App\Models\TeamProfile;

use App\Models\UserRoles;
use App\Models\UsersPositions;
use App\Models\WelcomeContent;
use Elibyy\TCPDF\Facades\TCPDF;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PDF;

class UserController extends Controller
{
    protected $roles;
    protected $userRoles;
    protected $challenges;
    protected $teamProfile;
    protected $maturityArea;
    protected $teamChallenges;
    protected $analyticMethods;
    protected $bestPracticeArea;
    protected $teamAnalyticMethods;
    protected $questionsAndAnswers;
    protected $welcomeContent;

    public function __construct()
    {
        $this->roles = new Roles;
        $this->userRoles = new UserRoles;
        $this->challenges = new Challenges;
        $this->teamProfile = new TeamProfile;
        $this->maturityArea = new MaturityArea;
        $this->teamChallenges = new TeamChallenges;
        $this->analyticMethods = new AnalyticMethods;
        $this->bestPracticeArea = new BestPracticeArea;
        $this->teamAnalyticMethods = new TeamAnalyticMethods;
        $this->questionsAndAnswers = new QuestionsAndAnswers;
        $this->welcomeContent = new WelcomeContent;
    }

    public function getTeamProfile()
    {
        $teamProfile = $this->teamProfile
            ->with('challenges', 'methods', 'roles')
            ->where('user_id', '=', Auth::user()->user_id)
            ->get();

        if (count($teamProfile)) {
            return response()->json(["response" => $teamProfile[0]], 200);
        } else {
            return response()->json(["message" => "Profile not found"], 400);
        }
    }

    /*
     * @input json array to update the profile of team and user about roles and challenges
     * @output response in json
     * */
    public function updateTeamProfile(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'organization' => 'required',
            'start_year' => 'required|digits:4',
            'sports' => 'required',
            'competition_levels' => 'required',
            'user_roles_company' => 'required',
            'challenges' => 'required|array',
            'user_roles_staff' => 'required|array',
            'analytic_methods' => 'required|array',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        $inputData = [
            "organization" => $request->input('organization'),
            "user_id" => Auth::user()->user_id,
            "start_year" => $request->input('start_year'),
            "sports" => $request->input('sports'),
            "competition_levels" => $request->input('competition_levels'),
            "user_roles_company" => $request->input('user_roles_company'),
        ];

        $teamResponse = $this->teamProfile->updateOrCreate([
            "user_id" => Auth::user()->user_id
        ], $inputData);

        $challenges = array_unique($request->input('challenges'));
        $userRolesStaff = array_unique($request->input('user_roles_staff'));
        $analyticMethods = array_unique($request->input('analytic_methods'));

        $this->userRoles->where('team_id', '=', $teamResponse->id)->delete();
        $this->teamChallenges->where('team_id', '=', $teamResponse->id)->delete();
        $this->teamAnalyticMethods->where('team_id', '=', $teamResponse->id)->delete();

        foreach ($userRolesStaff as $key => $val) {
            if ($val) {
                $this->userRoles->create(["team_id" => $teamResponse->id, "role_id" => $val]);
            }
        }

        foreach ($challenges as $key => $val) {
            if ($val) {
                $this->teamChallenges->create(["team_id" => $teamResponse->id, "challenge_id" => $val]);
            }
        }

        foreach ($analyticMethods as $key => $val) {
            if ($val) {
                $this->teamAnalyticMethods->create(["team_id" => $teamResponse->id, "analytic_methods_id" => $val]);
            }
        }

        return response()->json(["message" => "Team Profile Updated"], 200);
    }

    /*
     *
     * Function to update the answer for maturity questions
     * */
    public function updateMaturityAnswers(Request $request)
    {
        $input = $request->all();

        $messages = [
            "question_id.exists" => "Requested question is not available in our records.",
            "answer_id.exists" => "Requested answer is not available in our records.",
        ];

        $validator = Validator::make($input, [
            "question_id" => "required|exists:questions,id",
            "answer_id" => "required|exists:answers,id",
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        $userId = Auth::user()->user_id;

        try {
            $teamProfileId = $this->teamProfile->ofUser($userId)->first()->id;
        } catch (Exception $e) {
            return response()->json([
                "message" => "Error in trying to get profile of team. Please complete the team profile form first."
            ], 400);
        }

        $this->questionsAndAnswers->updateOrCreate([
            "team_profile_id" => $teamProfileId,
            "question_id" => $request->input('question_id')
        ], [
            "answer_id" => $request->input('answer_id')
        ]);

        return response()->json(["message" => "success"], 200);
    }

    /*
     * Function to return average score of maturity of any team based on user
     * */
    public function overallMaturityScore()
    {
        $userId = Auth::user()->user_id;

        $reports = $this->teamProfile->ofUser($userId)->with('reportedAnswer.answerValue')->first();

        $reports = $reports->toArray();

        $score = 0;

        $noOfQuestions = count($reports['reported_answer']);

        foreach ($reports['reported_answer'] as $key => $val) {
            $score += $val['answer_value']['score'];
        }

        $avg = $score == 0 ? 0 : round($score / $noOfQuestions);

        return response()->json(['score' => $avg, 'data' => maturity_level($avg)]);
    }

    /*
     * Function to get average score of surveyor's maturity categories
     * */
    public function maturityAreaScore()
    {
        $userId = Auth::user()->user_id;

        try {
            $teamProfileId = $this->teamProfile->ofUser($userId)->first()->id;
        } catch (Exception $e) {
            return response()->json([
                "message" => "Error in trying to get profile of team. Please complete the team profile form first."
            ], 400);
        }

        $data = $this->maturityArea
            ->select('maturity_area.name as maturity', 'maturity_area.id as maturity_id', 'maturity_area.text as maturity_text', 'maturity_area.definition', DB::raw('ROUND(AVG(answers.score)) as average'))
            ->leftJoin('best_practice_area', 'maturity_area.id', '=', 'best_practice_area.maturity_area_id')
            ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('question_and_answer', function ($q) use ($teamProfileId) {
                $q
                    ->on('questions.id', '=', 'question_and_answer.question_id')
                    ->where('question_and_answer.team_profile_id', '=', $teamProfileId);
            })
            ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
            ->groupBy('maturity_area.id')
            ->orderBy('maturity_area.name')
            ->get();

        foreach ($data AS $k => $v) {
            $data[$k]->data = maturity_level($v->average);
            $data[$k]->steps = $this->bestPracticeArea
                ->select('answers.result_text')
                ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
                ->leftJoin('question_and_answer', function ($q) use ($teamProfileId) {
                    $q
                        ->on('questions.id', '=', 'question_and_answer.question_id')
                        ->where('question_and_answer.team_profile_id', '=', $teamProfileId);
                })
                ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
                ->where('maturity_area_id', '=', $v->maturity_id)
                ->orderBy('answers.option', 'DESC')
                ->get();
        }

        $userLevel = $this->teamProfile->ofUser($userId)->first()->competition_levels;

        $others = $this->maturityArea
            ->select('maturity_area.name as maturity', 'maturity_area.definition', DB::raw('ROUND(AVG(answers.score), 2) as average'))
            ->leftJoin('best_practice_area', 'maturity_area.id', '=', 'best_practice_area.maturity_area_id')
            ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('question_and_answer', function ($q) {
                $q->on('questions.id', '=', 'question_and_answer.question_id');
            })
            ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
            ->leftJoin('team_profile', 'question_and_answer.team_profile_id', '=', 'team_profile.id')
            ->where('team_profile.competition_levels', '=', $userLevel)
            ->groupBy('maturity_area.id')
            ->orderBy('maturity_area.name')
            ->get();

        return response()->json(["response" => $data, "others" => $others]);
    }

    /*
     * Function to get average score of surveyor's best practice area
     * */
    public function bestPracticeAreaScore()
    {
        $userId = Auth::user()->user_id;

        try {
            $teamProfileId = $this->teamProfile->ofUser($userId)->first()->id;
        } catch (Exception $e) {
            return response()->json([
                "message" => "Error in trying to get profile of team. Please complete the team profile form first."
            ], 400);
        }

        $data = $this->bestPracticeArea
            ->select('best_practice_area.name as best_practice_area', 'best_practice_area.recommendation as practice_recommendation', 'maturity_area.name as maturity_area', DB::raw('ROUND(AVG(answers.score)) as average'))
            ->leftJoin('maturity_area', 'best_practice_area.maturity_area_id', '=', 'maturity_area.id')
            ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('question_and_answer', function ($q) use ($teamProfileId) {
                $q
                    ->on('questions.id', '=', 'question_and_answer.question_id')
                    ->where('question_and_answer.team_profile_id', '=', $teamProfileId);
            })
            ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
            ->groupBy('best_practice_area.name')
            ->orderBy('maturity_area.name')
            ->get();

        $userLevel = $this->teamProfile->ofUser($userId)->first()->competition_levels;

        $others = $this->bestPracticeArea
            ->select('best_practice_area.name as best_practice_area', 'maturity_area.name as maturity_area', DB::raw('ROUND(AVG(answers.score), 2) as average'))
            ->leftJoin('maturity_area', 'best_practice_area.maturity_area_id', '=', 'maturity_area.id')
            ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('question_and_answer', function ($q) {
                $q->on('questions.id', '=', 'question_and_answer.question_id');
            })
            ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
            ->leftJoin('team_profile', 'question_and_answer.team_profile_id', '=', 'team_profile.id')
            ->where('team_profile.competition_levels', '=', $userLevel)
            ->groupBy('best_practice_area.id')
            ->orderBy('maturity_area.name')
            ->get();

        return response()->json(["response" => $data, "others" => $others]);

    }

    /*
     * Function to get average score of over all surveys maturity categories
     * */
    public function overAllMaturityAreaScore()
    {
        $data = $this->maturityArea
            ->select('maturity_area.name as maturity', DB::raw('ROUND(AVG(answers.score), 2) as average'))
            ->leftJoin('best_practice_area', 'maturity_area.id', '=', 'best_practice_area.maturity_area_id')
            ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('question_and_answer', 'questions.id', '=', 'question_and_answer.question_id')
            ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
            ->groupBy('maturity_area.id')
            ->get();

        return response()->json(["response" => $data]);

    }

    /*
     * Function to get average score of surveyor's best practice area
     * */
    public function overAllBestPracticeAreaScore()
    {
        $data = $this->bestPracticeArea
            ->select('best_practice_area.name as best_practice_area', 'maturity_area.name as maturity_area', DB::raw('ROUND(AVG(answers.score), 2) as average'))
            ->leftJoin('maturity_area', 'best_practice_area.maturity_area_id', '=', 'maturity_area.id')
            ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('question_and_answer', 'questions.id', '=', 'question_and_answer.question_id')
            ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
            ->groupBy('best_practice_area.id')
            ->get();

        return response()->json(["response" => $data]);

    }

    /*
     * Function to get count of challenges having other teams and your team too
     * */
    public function otherTeamsChallenges()
    {
        $userId = Auth::user()->user_id;

        try {
            $teamProfileId = $this->teamProfile->ofUser($userId)->first()->id;
        } catch (Exception $e) {
            return response()->json([
                "message" => "Error in trying to get profile of team. Please complete the team profile form first."
            ], 400);
        }

        $challenges = $this->teamChallenges
            ->select('challenges.challenge')
            ->leftJoin('challenges', 'team_challenges.challenge_id', '=', 'challenges.id')
            ->where('team_id', '=', $teamProfileId)
            ->get();

        $teamCount = $this->teamChallenges->groupBy('team_id')->count();

        $data = $this->challenges
            ->select('challenges.id', 'challenges.challenge', DB::raw('ROUND(COUNT(team_challenges.challenge_id)/' . $teamCount . '*100, 2) as count'))
            ->join('team_challenges', 'team_challenges.challenge_id', '=', 'challenges.id', 'left outer')
            ->groupBy('challenges.id')
            ->orderBy('challenges.id')
            ->get();

        return response()->json([
            "response" => $data, "challenges" => $challenges
        ]);
    }

    /*
     * Function to get count of analytics methods having other teams
     * */
    public function otherTeamsAnalyticMethods()
    {
        $userId = Auth::user()->user_id;

        try {
            $teamProfileId = $this->teamProfile->ofUser($userId)->first()->id;
        } catch (Exception $e) {
            return response()->json([
                "message" => "Error in trying to get profile of team. Please complete the team profile form first."
            ], 400);
        }

        $methods = $this->teamAnalyticMethods
            ->select('analytic_methods.name')
            ->leftJoin('analytic_methods', 'team_analytic_methods.analytic_methods_id', '=', 'analytic_methods.id')
            ->where('team_id', '=', $teamProfileId)
            ->get();

        $teamCount = $this->teamAnalyticMethods->groupBy('team_id')->count();

        $data = $this->analyticMethods
            ->select('analytic_methods.id', 'analytic_methods.name', DB::raw('ROUND(COUNT(team_analytic_methods.analytic_methods_id)/' . $teamCount . '*100, 2) as count'))
            ->join('team_analytic_methods', 'team_analytic_methods.analytic_methods_id', '=', 'analytic_methods.id', 'left outer')
            ->groupBy('analytic_methods.id')
            ->orderBy('analytic_methods.id')
            ->get();

        return response()->json(["response" => $data, "methods" => $methods]);
    }

    public function otherUsersRoles()
    {
        $userId = Auth::user()->user_id;

        try {
            $teamProfileId = $this->teamProfile->ofUser($userId)->first()->id;
        } catch (Exception $e) {
            return response()->json([
                "message" => "Error in trying to get profile of team. Please complete the team profile form first."
            ], 400);
        }

        $userRoles = $this->userRoles
            ->select('roles.role')
            ->leftJoin('roles', 'user_roles.role_id', '=', 'roles.id')
            ->where('team_id', '=', $teamProfileId)
            ->get();

        $teamCount = $this->userRoles->groupBy('team_id')->count();

        $data = $this->roles
            ->select('roles.id', 'roles.role', DB::raw('ROUND(COUNT(user_roles.role_id)/' . $teamCount . '*100, 2) as count'))
            ->join('user_roles', 'user_roles.role_id', '=', 'roles.id', 'left outer')
            ->groupBy('roles.id')
            ->orderBy('roles.id')
            ->get();

        return response()->json(["response" => $data, "user_roles" => $userRoles]);
    }

    public function getUserPositions()
    {
        $userId = Auth::user()->user_id;

        $positions = (new UsersPositions)->where('user_id', '=', $userId)->get();

        return response()->json(['positions' => $positions], 200);
    }

    public function generatePdf()
    {
        $userId = Auth::user()->user_id;

        /*Overall scores*/

        $reports = $this->teamProfile->ofUser($userId)->with('reportedAnswer.answerValue')->first()->toArray();

        $score = 0;

        $noOfQuestions = count($reports['reported_answer']);

        foreach ($reports['reported_answer'] as $key => $val) {
            $score += $val['answer_value']['score'];
        }

        $avg = $score == 0 ? 0 : round($score / $noOfQuestions);

        /*Maturity area score*/

        try {
            $teamProfileId = $this->teamProfile->ofUser($userId)->first()->id;
        } catch (Exception $e) {
            return response()->json([
                "message" => "Error in trying to get profile of team. Please complete the team profile form first."
            ], 400);
        }

        $data = $this->maturityArea
            ->select('maturity_area.name as maturity', 'maturity_area.id as maturity_id', 'maturity_area.text as maturity_text', 'maturity_area.definition', DB::raw('ROUND(AVG(answers.score)) as average'))
            ->leftJoin('best_practice_area', 'maturity_area.id', '=', 'best_practice_area.maturity_area_id')
            ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('question_and_answer', function ($q) use ($teamProfileId) {
                $q
                    ->on('questions.id', '=', 'question_and_answer.question_id')
                    ->where('question_and_answer.team_profile_id', '=', $teamProfileId);
            })
            ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
            ->groupBy('maturity_area.id')
            ->orderBy('maturity_area.name')
            ->get();

        foreach ($data AS $k => $v) {
            $data[$k]->data = maturity_level($v->average);
            $data[$k]->steps = $this->bestPracticeArea
                ->select('answers.result_text')
                ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
                ->leftJoin('question_and_answer', function ($q) use ($teamProfileId) {
                    $q
                        ->on('questions.id', '=', 'question_and_answer.question_id')
                        ->where('question_and_answer.team_profile_id', '=', $teamProfileId);
                })
                ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
                ->where('maturity_area_id', '=', $v->maturity_id)
                ->orderBy('answers.option', 'DESC')
                ->get();
        }

        /* Best practice ares scores*/

        $practice_data = $this->bestPracticeArea
            ->select('best_practice_area.name as best_practice_area', 'best_practice_area.recommendation as practice_recommendation', 'maturity_area.name as maturity_area', DB::raw('ROUND(AVG(answers.score)) as average'))
            ->leftJoin('maturity_area', 'best_practice_area.maturity_area_id', '=', 'maturity_area.id')
            ->leftJoin('questions', 'best_practice_area.id', '=', 'questions.best_practice_area_id')
            ->leftJoin('question_and_answer', function ($q) use ($teamProfileId) {
                $q
                    ->on('questions.id', '=', 'question_and_answer.question_id')
                    ->where('question_and_answer.team_profile_id', '=', $teamProfileId);
            })
            ->leftJoin('answers', 'question_and_answer.answer_id', '=', 'answers.id')
            ->groupBy('best_practice_area.id')
            ->orderBy('maturity_area.name')
            ->get();

        $team_name = $this->teamProfile->where('user_id', '=', $userId)->get();

        $fileName = $userId . '_' . time();
        $filePath = url('/') . '/storage/app/public/exports/' . $fileName;

        $view = View::make('pdf.assesment-view', ['overall_score' => $avg, 'data' => maturity_level($avg), 'Maturity_area' => $data, 'practice_area' => $practice_data, 'team_name' => $team_name[0]]);
        $html = $view->render();

        PDF::SetTitle('Agile Sports Analytics');
        PDF::SetMargins(10, 10, 12);
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output(base_path() . '/storage/app/public/exports/' . $fileName . '.pdf', 'F');
        return response()->json(['url' => $filePath . '.pdf']);
    }

    public function getPageWelcomeContent()
    {
        $welcomeContent = $this->welcomeContent->get();
        return array('response' => $welcomeContent->first());
    }

}
