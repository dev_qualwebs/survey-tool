<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnalyticMethods extends Model
{
    use SoftDeletes;

    protected $table = 'analytic_methods';

    protected $fillable = ['name'];

    public $timestamps = false;

    protected $hidden = [
        'deleted_at'
    ];
}
