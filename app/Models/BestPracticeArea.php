<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BestPracticeArea extends Model
{
    use SoftDeletes;

    protected $table = 'best_practice_area';

    protected $fillable = ['maturity_area_id', 'name', 'recommendation'];

    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at',
    ];

    public function maturityArea()
    {
        return $this->belongsTo(MaturityArea::class, 'maturity_area_id' ,'id');
    }

    public function questions()
    {
        return $this->hasMany(Questions::class, 'best_practice_area_id', 'id');
    }
}
