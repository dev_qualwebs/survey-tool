<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaturityArea extends Model
{
    use SoftDeletes;

    protected $table = 'maturity_area';

    protected $fillable = ['name', 'definition', 'text'];

    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at',
    ];

    public function bestPracticeArea()
    {
        return $this->hasMany(BestPracticeArea::class, 'maturity_area_id', 'id');
    }
}
