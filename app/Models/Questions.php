<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Questions extends Model
{
    use SoftDeletes;

    protected $table = 'questions';

    protected $fillable = ['best_practice_area_id', 'question'];

    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at',
    ];

    public function options()
    {
        return $this->hasMany(Answers::class, 'question_id', 'id');
    }

    public function selected()
    {
        return $this->hasOne(QuestionsAndAnswers::class, 'question_id', 'id');
    }
}
