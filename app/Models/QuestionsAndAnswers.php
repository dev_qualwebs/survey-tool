<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionsAndAnswers extends Model
{
    protected $table = 'question_and_answer';

    protected $fillable = ["team_profile_id", "question_id", "answer_id"];

    public function answerValue()
    {
        return $this->belongsTo(Answers::class, 'answer_id', 'id')
            ->select('id', 'option', 'score');
    }
}
