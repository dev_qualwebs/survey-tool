<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamAnalyticMethods extends Model
{
    protected $table = 'team_analytic_methods';

    protected $fillable = ['team_id', 'analytic_methods_id'];
}
