<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamChallenges extends Model
{
    protected $table = 'team_challenges';

    protected $fillable = ['team_id', 'challenge_id'];
}
