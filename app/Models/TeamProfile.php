<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TeamProfile extends Model
{
    protected $table = 'team_profile';

    protected $fillable = ['user_id', 'start_year', 'sports', 'competition_levels', 'user_roles_company', 'organization'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    public function reportedAnswer()
    {
        return $this->hasMany(QuestionsAndAnswers::class, 'team_profile_id', 'id');
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfUser($query, $userId)
    {
        return $query->where('user_id', '=', $userId);
    }

    public function challenges()
    {
        return $this->hasMany(TeamChallenges::class, 'team_id', 'id');
    }

    public function methods()
    {
        return $this->hasMany(TeamAnalyticMethods::class, 'team_id', 'id');
    }

    public function roles()
    {
        return $this->hasMany(UserRoles::class, 'team_id', 'id');
    }
}
