<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersPositions extends Model
{
    protected $table = 'users_positions';

    protected $fillable = ["user_id", "company_name", "title", "isCurrent", "start_month", "start_year"];
}
