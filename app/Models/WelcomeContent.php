<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WelcomeContent extends Model
{
   protected $table = 'welcome_content';

   protected $fillable = ["heading_1", "heading_2", "bottom_content", "image", "home_page_link", "sports_analytics_link", "elit_athlete_link", "attention_coaches_link", "contact_us_link"];
}
