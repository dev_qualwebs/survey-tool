<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id', 190)->unique();
            $table->string('name');
            $table->string('email', 190)->unique();
            $table->string('password');
            $table->string('linkedin_id')->nullable();
            $table->string('facebook_id')->nullable();
            $table->tinyInteger('type')->comment('0 User 1 Admin')->default(0);
            $table->text('token')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
