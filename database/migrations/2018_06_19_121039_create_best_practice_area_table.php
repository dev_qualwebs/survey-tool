<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBestPracticeAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('best_practice_area', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('maturity_area_id');
            $table->string('name');
            $table->text('recommendation');
            $table->softDeletes();
            $table->foreign('maturity_area_id')->references('id')->on('maturity_area')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('best_practice_area');
    }
}
