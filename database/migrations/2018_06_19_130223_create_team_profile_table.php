<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->string('organization');
            $table->string('user_id', 190);
            $table->year('start_year');
            $table->string('sports');
            $table->string('competition_levels');
            $table->string('user_roles_company')->comment('User\'s role at company');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_profile');
    }
}
