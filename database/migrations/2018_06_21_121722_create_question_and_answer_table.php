<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionAndAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_and_answer', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('team_profile_id');
            $table->unsignedInteger('question_id');
            $table->unsignedInteger('answer_id');
            $table->timestamps();
            $table->foreign('team_profile_id')->references('id')->on('team_profile')->onDelete('cascade');
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('cascade');
            $table->unique(['team_profile_id', 'question_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_and_answer');
    }
}
