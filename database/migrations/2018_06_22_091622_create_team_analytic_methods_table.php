<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamAnalyticMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_analytic_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('team_id');
            $table->unsignedInteger('analytic_methods_id');
            $table->timestamps();
            $table->foreign('analytic_methods_id')->references('id')->on('analytic_methods')->onDelete('cascade');
            $table->foreign('team_id')->references('id')->on('team_profile')->onDelete('cascade');
            $table->unique(['team_id', 'analytic_methods_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_analytic_methods');
    }
}
