<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWelcomeContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('welcome_content', function (Blueprint $table) {
            $table->increments('id');
            $table->text('heading_1');
            $table->text('heading_2');
            $table->text('bottom_content');
            $table->string('image');
            $table->text('home_page_link');
            $table->text('sports_analytics_link');
            $table->text('attention_coaches_link');
            $table->text('elit_athlete_link');
            $table->text('contact_us_link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('welcome_content');
    }
}
