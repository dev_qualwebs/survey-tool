<?php

use App\User;
use Illuminate\Database\Seeder;

class AddAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $res = (new User)->where('name', '=', 'admin')->get();
        if (count($res)) {
            $alreadyAdd = true;
        } else {
            $alreadyAdd = false;
        }

        if (!$alreadyAdd) {
            $userId = generateRandomString();
            $isUserIdAvailable = !!count((new User)->where('user_id', '=', $userId)->get());

            while ($isUserIdAvailable) {
                $userId = generateRandomString();
                $isUserIdAvailable = !!count((new User)->where('user_id', '=', $userId)->get());
            }
            
            (new User)->create([
                'user_id' => $userId,
                'name' => 'admin',
                'email' => 'admin@surveytool.com',
                'password' => Hash::make('admin'),
                'type' => 1
            ]);
        }
    }
}
