<?php

use App\Models\AnalyticMethods;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnalyticMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $analyticsMethods =
            [
                ["name" => "Frequency Distribution"],
                ["name" => "Conditional/Unconditional Probability"],
                ["name" => "Binomial and Normal Distribution"],
                ["name" => "Auto-Correlation for Pattern Detection"],
                ["name" => "Mathematical Programming"],
                ["name" => "Classical Statistics"],
                ["name" => "Bayesian Statistics"],
                ["name" => "Regression and Classification"],
                ["name" => "Neural Networks"],
                ["name" => "Heuristic Algorithms Simulation"],
                ["name" => "Decision Analysis"],
                ["name" => "Multivariate Statistics"],
                ["name" => "Machine Learning"],
                ["name" => "Text and Sentiment Analysis"],
                ["name" => "Sales Forecasting"],
                ["name" => "Market Response Models"],
                ["name" => "Social Network Analysis"],
                ["name" => "Data Visualization"],
            ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        AnalyticMethods::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        AnalyticMethods::insert($analyticsMethods);
    }
}
