<?php

use App\Models\Answers;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnswersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "option" => "A",
                "text" => "We have improved the most among teams in our division.",
                "score" => 100,
                "result_text" => "Continue applying your continuous improvement and adaptation model around value and analytics.",
                "question_id" => 1
            ],
            [
                "option" => "B",
                "text" => "We are among the top teams in terms of improvement.",
                "score" => 75,
                "result_text" => "Continue applying your continuous improvement and adaptation model around value and analytics.",
                "question_id" => 1
            ],
            [
                "option" => "C",
                "text" => "We're hitting our goals.",
                "score" => 50,
                "result_text" => "Implement a continuous improvement and adaptation model built around value and analytics in order to gain traction in your division.",
                "question_id" => 1
            ],
            [
                "option" => "D",
                "text" => "We have not improved our standing.",
                "score" => 25,
                "result_text" => "Implement a continuous improvement and adaptation model built around value and analytics in order to gain traction in your division.",
                "question_id" => 1
            ],
            [
                "option" => "E",
                "text" => "We have declined in our performance and standings.",
                "score" => 0,
                "result_text" => "Implement a continuous improvement and adaptation model built around value and analytics in order to gain traction in your division.",
                "question_id" => 1
            ],

            [
                "option" => "A",
                "text" => "Yes, our analytics program is best in class among our league competitors.",
                "score" => 100,
                "result_text" => "Continue seeking opportunities to leverage analytics to gaining competitive edge.",
                "question_id" => 2
            ],
            [
                "option" => "B",
                "text" => "Our analytics program yields performance gains that give us a slight advantage.",
                "score" => 75,
                "result_text" => "Continue seeking opportunities to leverage analytics to gaining competitive edge.",
                "question_id" => 2
            ],
            [
                "option" => "C",
                "text" => "Our results are about the same as other teams that are invested in analytics.",
                "score" => 50,
                "result_text" => "Seek opportunities to leverage analytics to gain a competitive edge.",
                "question_id" => 2
            ],
            [
                "option" => "D",
                "text" => "Our analytics program is not very developed, and performance gains cannot yet be quantified.",
                "score" => 25,
                "result_text" => "Seek opportunities to leverage analytics to gain a competitive edge.",
                "question_id" => 2
            ],
            [
                "option" => "E",
                "text" => "Our lack of analytics gives our team a distinct disadvantage.",
                "score" => 0,
                "result_text" => "Seek opportunities to leverage analytics to gain a competitive edge.",
                "question_id" => 2
            ],

            [
                "option" => "A",
                "text" => "All the time",
                "score" => 100,
                "result_text" => "Continue applying analytics to measure present value and future value of contracts. Discover new ways to negotiate performance and signing bonuses.",
                "question_id" => 3
            ],
            [
                "option" => "B",
                "text" => "Whenever possible",
                "score" => 75,
                "result_text" => "Continue applying analytics to measure present value and future value of contracts. Discover new ways to negotiate performance and signing bonuses.",
                "question_id" => 3
            ],
            [
                "option" => "C",
                "text" => "Sometimes",
                "score" => 50,
                "result_text" => "Apply analytics to determine the present value and future value of contracts and to negotiate performance and signing bonuses.",
                "question_id" => 3
            ],
            [
                "option" => "D",
                "text" => "No, but we'd like to.",
                "score" => 25,
                "result_text" => "Apply analytics to determine the present value and future value of contracts and to negotiate performance and signing bonuses.",
                "question_id" => 3
            ],
            [
                "option" => "E",
                "text" => "We don't see value in analytics regarding contracts.",
                "score" => 0,
                "result_text" => "Apply analytics to determine the present value and future value of contracts and to negotiate performance and signing bonuses.",
                "question_id" => 3
            ],

            [
                "option" => "A",
                "text" => "Strongly agree",
                "score" => 100,
                "result_text" => "Continue applying new analytic trends that help improve performance, predict future outcomes and evaluate the market value of players.",
                "question_id" => 4
            ],
            [
                "option" => "B",
                "text" => "Agree",
                "score" => 75,
                "result_text" => "Continue applying new analytic trends that help improve performance, predict future outcomes and evaluate the market value of players.",
                "question_id" => 4
            ],
            [
                "option" => "C",
                "text" => "Hit or miss",
                "score" => 50,
                "result_text" => "Apply new analytic trends that help improve performance, predict future outcomes and evaluate the market value of players.",
                "question_id" => 4
            ],
            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Apply new analytic trends that help improve performance, predict future outcomes and evaluate the market value of players.",
                "question_id" => 4
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree",
                "score" => 0,
                "result_text" => "Apply new analytic trends that help improve performance, predict future outcomes and evaluate the market value of players.",
                "question_id" => 4
            ],

            [
                "option" => "A",
                "text" => "Predictive analytics",
                "score" => 100,
                "result_text" => "Continue identifying and integrating people, processes and systems that will push your reporting capabilities to predictive analytics.",
                "question_id" => 5
            ],
            [
                "option" => "B",
                "text" => "Integrated analytics",
                "score" => 75,
                "result_text" => "Continue identifying and integrating people, processes and systems that will push your reporting capabilities to predictive analytics.",
                "question_id" => 5
            ],
            [
                "option" => "C",
                "text" => "Silo'd analytics (team, department)",
                "score" => 50,
                "result_text" => "Identify and integrate people, processes and systems that will push your reporting capabilities from reactive and/or siloed reporting to predictive analytics.",
                "question_id" => 5
            ],
            [
                "option" => "D",
                "text" => "Proactive Reporting (KPI reporting)",
                "score" => 25,
                "result_text" => "Identify and integrate people, processes and systems that will push your reporting capabilities from reactive and/or siloed reporting to predictive analytics.",
                "question_id" => 5
            ],
            [
                "option" => "E",
                "text" => "Passive reporting (canned reports and dashboards)",
                "score" => 0,
                "result_text" => "Identify and integrate people, processes and systems that will push your reporting capabilities from reactive and/or siloed reporting to predictive analytics.",
                "question_id" => 5
            ],
            [
                "option" => "A",
                "text" => "Strongly agree",
                "score" => 100,
                "result_text" => "Continue implementing data security best practices that ensure data accuracy and trustworthiness.",
                "question_id" => 6
            ],
            [
                "option" => "B",
                "text" => "Agree",
                "score" => 75,
                "result_text" => "Continue implementing data security best practices that ensure data accuracy and trustworthiness.",
                "question_id" => 6
            ],
            [
                "option" => "C",
                "text" => "Hit or miss",
                "score" => 50,
                "result_text" => "Implement data security best practices that ensure your data is accurate and trustworthy.",
                "question_id" => 6
            ],
            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Implement data security best practices that ensure your data is accurate and trustworthy.",
                "question_id" => 6
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree",
                "score" => 0,
                "result_text" => "Implement data security best practices that ensure your data is accurate and trustworthy.",
                "question_id" => 6
            ],


            [
                "option" => "A",
                "text" => "Strongly agree",
                "score" => 100,
                "result_text" => "Keep focus on analyzing data that is meaningful, which the team can take immediate actions to improve performance.",
                "question_id" => 7
            ],
            [
                "option" => "B",
                "text" => "Agree",
                "score" => 75,
                "result_text" => "Keep focus on analyzing data that is meaningful, which the team can take immediate actions to improve performance.",
                "question_id" => 7
            ],
            [
                "option" => "C",
                "text" => "It's adequate",
                "score" => 50,
                "result_text" => "Focus on analyzing data that is meaningful, which the team can take immediate actions to improve performance.",
                "question_id" => 7
            ],
            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Focus on analyzing data that is meaningful, which the team can take immediate actions to improve performance.",
                "question_id" => 7
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree",
                "score" => 0,
                "result_text" => "Focus on analyzing data that is meaningful, which the team can take immediate actions to improve performance.",
                "question_id" => 7
            ],

            [
                "option" => "A",
                "text" => "Strongly agree",
                "score" => 100,
                "result_text" => "Keep tracking player movement to understand how to leverage spacing, distance and speed to gain the greatest competitive edge.",
                "question_id" => 8
            ],
            [
                "option" => "B",
                "text" => "Agree",
                "score" => 75,
                "result_text" => "Keep tracking player movement to understand how to leverage spacing, distance and speed to gain the greatest competitive edge.",
                "question_id" => 8
            ],
            [
                "option" => "C",
                "text" => "Not Sure",
                "score" => 50,
                "result_text" => "Track player movement to understand how to leverage spacing, distance and speed to gain the greatest competitive edge.",
                "question_id" => 8
            ],
            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Track player movement to understand how to leverage spacing, distance and speed to gain the greatest competitive edge.",
                "question_id" => 8
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree",
                "score" => 0,
                "result_text" => "Track player movement to understand how to leverage spacing, distance and speed to gain the greatest competitive edge.",
                "question_id" => 8
            ],

            [
                "option" => "A",
                "text" => "We offer a unique product on or off the court, and that's why fans like us.",
                "score" => 100,
                "result_text" => "Maintain your team’s unique identity and the unique characteristics that drive you towards your team goals.",
                "question_id" => 9
            ],
            [
                "option" => "B",
                "text" => "We have some important differences that offer a competitive advantage.",
                "score" => 75,
                "result_text" => "Maintain your team’s unique identity and the unique characteristics that drive you towards your team goals.",
                "question_id" => 9
            ],
            [
                "option" => "C",
                "text" => "We have a unique difference on or off the court that draws some fans.",
                "score" => 50,
                "result_text" => "Define your team’s unique identity and the unique characteristics that will drive you towards your team goals.",
                "question_id" => 9
            ],
            [
                "option" => "D",
                "text" => "We have a few small things that set us apart, which draw a few fans",
                "score" => 25,
                "result_text" => "Define your team’s unique identity and the unique characteristics that will drive you towards your team goals.",
                "question_id" => 9
            ],
            [
                "option" => "E",
                "text" => "Fans are not drawn to us because of our unique identity or service differentiation.",
                "score" => 0,
                "result_text" => "Define your team’s unique identity and the unique characteristics that will drive you towards your team goals.",
                "question_id" => 9
            ],

            [
                "option" => "A",
                "text" => "We have so many fans and sponsorships, our challenge is maintaining this level of satisfaction.",
                "score" => 100,
                "result_text" => "Continue seeking opportunities to diversify your fans and sponsorships.",
                "question_id" => 10
            ],
            [
                "option" => "B",
                "text" => "We can lose a few fans and sponsors for the right cause and still be fine.",
                "score" => 75,
                "result_text" => "Continue seeking opportunities to diversify your fans and sponsorships.",
                "question_id" => 10
            ],
            [
                "option" => "C",
                "text" => "We can lose a few fans and sponsors for the right cause, but we'd feel it.",
                "score" => 50,
                "result_text" => "Seek opportunities to diversify your fans and sponsorships.",
                "question_id" => 10
            ],
            [
                "option" => "D",
                "text" => "It would be really tough if we lost a major sponsor.",
                "score" => 25,
                "result_text" => "Seek opportunities to diversify your fans and sponsorships.",
                "question_id" => 10
            ],
            [
                "option" => "E",
                "text" => "In any of our fans or sponsors left, we'd be at risk of being profitable.",
                "score" => 0,
                "result_text" => "Seek opportunities to diversify your fans and sponsorships.",
                "question_id" => 10
            ],

            [
                "option" => "A",
                "text" => "Yes, analytics tell us exactly how we're doing with our fans at any point in time.",
                "score" => 100,
                "result_text" => "Continue applying analytics to track key fan satisfaction performance indicators and exceeding fan expectations",
                "question_id" => 11
            ],
            [
                "option" => "B",
                "text" => "We have a pretty good idea most of the time.",
                "score" => 75,
                "result_text" => "Continue applying analytics to track key fan satisfaction performance indicators and exceeding fan expectations",
                "question_id" => 11
            ],
            [
                "option" => "C",
                "text" => "We have the ability to check when we need to.",
                "score" => 50,
                "result_text" => "Apply analytics to track key fan satisfaction performance indicators and exceed fan expectations",
                "question_id" => 11
            ],
            [
                "option" => "D",
                "text" => "Our analytics do not provide valuable insight into fan expectations.",
                "score" => 25,
                "result_text" => "Apply analytics to track key fan satisfaction performance indicators and exceed fan expectations",
                "question_id" => 11
            ],
            [
                "option" => "E",
                "text" => "We do not have analytics that capture fan experience and satisfaction.",
                "score" => 0,
                "result_text" => "Apply analytics to track key fan satisfaction performance indicators and exceed fan expectations",
                "question_id" => 11
            ],

            [
                "option" => "A",
                "text" => "We always find, develop and retain quality players who contribute to reaching our goals.",
                "score" => 100,
                "result_text" => "Keep applying analytics to find, develop and retain quality players that meet the team's needs and drive team goals.",
                "question_id" => 12
            ],
            [
                "option" => "B",
                "text" => "Yes, we have great success using analytics to find, develop and retain the best players to fill our needs and reach goals.",
                "score" => 75,
                "result_text" => "Keep applying analytics to find, develop and retain quality players that meet the team's needs and drive team goals.",
                "question_id" => 12
            ],
            [
                "option" => "C",
                "text" => "Yes, we have some success using analytics to find, develop and retain the best players to fill our needs and reach goals.",
                "score" => 50,
                "result_text" => "Apply analytics to find, develop and retain quality players that will meet the team's needs and drive team goals.",
                "question_id" => 12
            ],
            [
                "option" => "D",
                "text" => "It's hit or miss. We've made some poor decisions.",
                "score" => 25,
                "result_text" => "Apply analytics to find, develop and retain quality players that will meet the team's needs and drive team goals.",
                "question_id" => 12
            ],
            [
                "option" => "E",
                "text" => "We don't use analytics to acquire and develop talent.",
                "score" => 0,
                "result_text" => "Apply analytics to find, develop and retain quality players that will meet the team's needs and drive team goals.",
                "question_id" => 12
            ],

            [
                "option" => "A",
                "text" => "Yes, our senior leadership relies heavily on analytics to make decisions.",
                "score" => 100,
                "result_text" => "Keep leveraging analytics for senior leadership decision making.",
                "question_id" => 13
            ],

            [
                "option" => "B",
                "text" => "Yes, senior leadership uses analytics, but analytics don't drive decision making.",
                "score" => 75,
                "result_text" => "Keep leveraging analytics for senior leadership decision making.",
                "question_id" => 13
            ],
            [
                "option" => "C",
                "text" => "Senior leadership is in the process of leveraging analytics to make decisions.",
                "score" => 50,
                "result_text" => "Leverage analytics for senior leadership decision making.",
                "question_id" => 13
            ],
            [
                "option" => "D",
                "text" => "Senior leadership does not use analytics for decision making, but plan to in near future.",
                "score" => 25,
                "result_text" => "Leverage analytics for senior leadership decision making.",
                "question_id" => 13
            ],
            [
                "option" => "E",
                "text" => "Senior leadership does not use analytics, and does not currently plan to.",
                "score" => 0,
                "result_text" => "Leverage analytics for senior leadership decision making.",
                "question_id" => 13
            ],

            [
                "option" => "A",
                "text" => "Data scientist, expert data modelers and statisticians on staff. Complex problem solvers.",
                "score" => 100,
                "result_text" => "Keep building your team of data scientists, expert data modelers and statisticians that generate deep analysis and analytics.",
                "question_id" => 14
            ],
            [
                "option" => "B",
                "text" => "Analytical modelers and statisticians on staff.",
                "score" => 75,
                "result_text" => "Keep building your team of data scientists, expert data modelers and statisticians that generate deep analysis and analytics.",
                "question_id" => 14
            ],
            [
                "option" => "C",
                "text" => "Few analysts and limited usage of advanced analytics.",
                "score" => 50,
                "result_text" => "Build a team of data scientists, expert data modelers and statisticians that can generate deep analysis and analytics.",
                "question_id" => 14
            ],
            [
                "option" => "D",
                "text" => "Functional knowledge for BI tools.",
                "score" => 25,
                "result_text" => "Build a team of data scientists, expert data modelers and statisticians that can generate deep analysis and analytics.",
                "question_id" => 14
            ],
            [
                "option" => "E",
                "text" => "No formal analytic knowledge or expertise. We're winging it.",
                "score" => 0,
                "result_text" => "Build a team of data scientists, expert data modelers and statisticians that can generate deep analysis and analytics.",
                "question_id" => 14
            ],

            [
                "option" => "A",
                "text" => "Strongly agree.",
                "score" => 100,
                "result_text" => "Continue explaining results carefully to team members and organizational staff in terms they can understand, and clearly demonstrate the progress that has been made.",
                "question_id" => 15
            ],
            [
                "option" => "B",
                "text" => "Agree",
                "score" => 75,
                "result_text" => "Continue explaining results carefully to team members and organizational staff in terms they can understand, and clearly demonstrate the progress that has been made.",
                "question_id" => 15
            ],
            [
                "option" => "C",
                "text" => "They do adequate job explaining analytics.",
                "score" => 50,
                "result_text" => "Create a requirement in your process where analysts must explain results carefully in terms that the team and organization can understand, and clearly demonstrates the progress that has been made.",
                "question_id" => 15
            ],
            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Create a requirement in your process where analysts must explain results carefully in terms that the team and organization can understand, and clearly demonstrates the progress that has been made.",
                "question_id" => 15
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree.",
                "score" => 0,
                "result_text" => "Create a requirement in your process where analysts must explain results carefully in terms that the team and organization can understand, and clearly demonstrates the progress that has been made.",
                "question_id" => 15
            ],

            [
                "option" => "A",
                "text" => "Strongly agree.",
                "score" => 100,
                "result_text" => "Keep building your strategy and process to enable the team to play with synergy on offense and defense, utilize spacing effectively and self-organize. Self-organization is a team state that is established when full transparency, inspection, and adaptation are achieved, and the goal of the team is placed above the goals of individuals.",
                "question_id" => 16
            ],
            [
                "option" => "B",
                "text" => "Agree",
                "score" => 75,
                "result_text" => "Keep building your strategy and process to enable the team to play with synergy on offense and defense, utilize spacing effectively and self-organize. Self-organization is a team state that is established when full transparency, inspection, and adaptation are achieved, and the goal of the team is placed above the goals of individuals.",
                "question_id" => 16
            ],
            [
                "option" => "C",
                "text" => "Not Sure",
                "score" => 50,
                "result_text" => "Implement a strategy and process to enable the team to play with synergy on offense and defense, utilize spacing effectively and self-organize. Self-organization is a team state that is established when full transparency, inspection, and adaptation are achieved, and the goal of the team is placed above the goals of individuals.",
                "question_id" => 16
            ],
            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Implement a strategy and process to enable the team to play with synergy on offense and defense, utilize spacing effectively and self-organize. Self-organization is a team state that is established when full transparency, inspection, and adaptation are achieved, and the goal of the team is placed above the goals of individuals.",
                "question_id" => 16
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree.",
                "score" => 0,
                "result_text" => "Implement a strategy and process to enable the team to play with synergy on offense and defense, utilize spacing effectively and self-organize. Self-organization is a team state that is established when full transparency, inspection, and adaptation are achieved, and the goal of the team is placed above the goals of individuals.",
                "question_id" => 16
            ],

            [
                "option" => "A",
                "text" => "Strongly agree.",
                "score" => 100,
                "result_text" => "Keep discovering new ways to enable the coaching staff to teach players how to understand and leverage analytics, create value and execute the game plan.",
                "question_id" => 17
            ],
            [
                "option" => "B",
                "text" => "Agree",
                "score" => 75,
                "result_text" => "Keep discovering new ways to enable the coaching staff to teach players how to understand and leverage analytics, create value and execute the game plan.",
                "question_id" => 17
            ],
            [
                "option" => "C",
                "text" => "Not Sure",
                "score" => 50,
                "result_text" => "Implement a system to enable the coaching staff to teach players how to understand and leverage analytics, create value and execute the game plan.",
                "question_id" => 17
            ],
            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Implement a system to enable the coaching staff to teach players how to understand and leverage analytics, create value and execute the game plan.",
                "question_id" => 17
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree.",
                "score" => 0,
                "result_text" => "Implement a system to enable the coaching staff to teach players how to understand and leverage analytics, create value and execute the game plan.",
                "question_id" => 17
            ],

            [
                "option" => "A",
                "text" => "Strongly agree.",
                "score" => 100,
                "result_text" => "Continue implementing processes and training so that players understand the variables and algorithms behind your metrics, and exactly how they can add value to reach team goals.",
                "question_id" => 18
            ],
            [
                "option" => "B",
                "text" => "Agree",
                "score" => 75,
                "result_text" => "Continue implementing processes and training so that players understand the variables and algorithms behind your metrics, and exactly how they can add value to reach team goals.",
                "question_id" => 18
            ],
            [
                "option" => "C",
                "text" => "Not Sure",
                "score" => 50,
                "result_text" => "Implement processes and training so that players understand the variables and algorithms behind your metrics and exactly how they can add value to reach team goals.",
                "question_id" => 18
            ],

            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Implement processes and training so that players understand the variables and algorithms behind your metrics and exactly how they can add value to reach team goals.",
                "question_id" => 18
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree.",
                "score" => 0,
                "result_text" => "Implement processes and training so that players understand the variables and algorithms behind your metrics and exactly how they can add value to reach team goals.",
                "question_id" => 18
            ],


            [
                "option" => "A",
                "text" => "We have seen revenue growth year in and year out.",
                "score" => 100,
                "result_text" => "Keep identifying new avenue to create and grow revenues.",
                "question_id" => 19
            ],
            [
                "option" => "B",
                "text" => "We are among the top teams in our league, in terms of revenues.",
                "score" => 75,
                "result_text" => "Keep identifying new avenue to create and grow revenues.",
                "question_id" => 19
            ],
            [
                "option" => "C",
                "text" => "We're hitting our goals.",
                "score" => 50,
                "result_text" => "Identify new avenues to create and grow revenues.",
                "question_id" => 19
            ],
            [
                "option" => "D",
                "text" => "We have not grown as desired over the last three years.",
                "score" => 25,
                "result_text" => "Identify new avenues to create and grow revenues.",
                "question_id" => 19
            ],
            [
                "option" => "E",
                "text" => "We have fallen far short of our revenue goals.",
                "score" => 0,
                "result_text" => "Identify new avenues to create and grow revenues.",
                "question_id" => 19
            ],

            [
                "option" => "A",
                "text" => "We have long term contractual comittments for all areas contracts can be secured.",
                "score" => 100,
                "result_text" => "Continue seeking long term contract opportunities to secure future revenues.",
                "question_id" => 20
            ],
            [
                "option" => "B",
                "text" => "We have long term contractual comittments for most areas contracts can be secured.",
                "score" => 75,
                "result_text" => "Continue seeking long term contract opportunities to secure future revenues.",
                "question_id" => 20
            ],
            [
                "option" => "C",
                "text" => "We have some steady contracts and other areas we're exploring contractual revenues.",
                "score" => 50,
                "result_text" => "Seek long term contract opportunities to secure future revenues when possible.",
                "question_id" => 20
            ],
            [
                "option" => "D",
                "text" => "We have only a few contracts and not many areas identified for future contractual revenues.",
                "score" => 25,
                "result_text" => "Seek long term contract opportunities to secure future revenues when possible.",
                "question_id" => 20
            ],
            [
                "option" => "E",
                "text" => "We're not very confident in our future revenue. ",
                "score" => 0,
                "result_text" => "Seek long term contract opportunities to secure future revenues when possible.",
                "question_id" => 20
            ],

            [
                "option" => "A",
                "text" => "Analytics drive our sales and marketing processes and profits.",
                "score" => 100,
                "result_text" => "Continue molding a proven and systematic method for leveraging sales and marketing analytics, and gaining sustainable profits.",
                "question_id" => 21
            ],
            [
                "option" => "B",
                "text" => "Sales and marketing analytics are applied and we are gaining insight into opportunities for revenue growth.",
                "score" => 75,
                "result_text" => "Continue molding a proven and systematic method for leveraging sales and marketing analytics, and gaining sustainable profits.",
                "question_id" => 21
            ],
            [
                "option" => "C",
                "text" => "We dapbble with sales and marketing analytics, but don't know how to analyze our data.",
                "score" => 50,
                "result_text" => "Establish proven and systematic methods for leveraging sales and marketing analytics and gaining sustainable profits.",
                "question_id" => 21
            ],
            [
                "option" => "D",
                "text" => "We are in the process of implementing analytics into our sales and marketing processes.",
                "score" => 25,
                "result_text" => "Establish proven and systematic methods for leveraging sales and marketing analytics and gaining sustainable profits.",
                "question_id" => 21
            ],
            [
                "option" => "E",
                "text" => "Sales and marketing analytics are not in our near term plan.",
                "score" => 0,
                "result_text" => "Establish proven and systematic methods for leveraging sales and marketing analytics and gaining sustainable profits.",
                "question_id" => 21
            ],

            [
                "option" => "A",
                "text" => "Strongly agree",
                "score" => 100,
                "result_text" => "Keep utilizing sales analytics to understand which fans are more likely to buy, what they are more likely to buy, and when they are likely to buy tickets, merchandise, food and promotions.",
                "question_id" => 22
            ],

            [
                "option" => "B",
                "text" => "Strongly",
                "score" => 75,
                "result_text" => "Keep utilizing sales analytics to understand which fans are more likely to buy, what they are more likely to buy, and when they are likely to buy tickets, merchandise, food and promotions.",
                "question_id" => 22
            ],
            [
                "option" => "C",
                "text" => "Hit or miss",
                "score" => 50,
                "result_text" => "Utilize sales analytics to understand which fans are more likely to buy, what they are more likely to buy, and when they are likely to buy tickets, merchandise, food and promotions.",
                "question_id" => 22
            ],
            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Utilize sales analytics to understand which fans are more likely to buy, what they are more likely to buy, and when they are likely to buy tickets, merchandise, food and promotions.",
                "question_id" => 22
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree",
                "score" => 0,
                "result_text" => "Utilize sales analytics to understand which fans are more likely to buy, what they are more likely to buy, and when they are likely to buy tickets, merchandise, food and promotions.",
                "question_id" => 22
            ],


            [
                "option" => "A",
                "text" => "Yes, the entire organization understands the role analytics play in reaching our team/organizational goals.",
                "score" => 100,
                "result_text" => "Continue providing a holistic understanding of your team's mission and goals and how analytics contribute to achieving them.",
                "question_id" => 23
            ],
            [
                "option" => "B",
                "text" => "Most of the organization understands the role analytics play in reaching our team/organizational goals.",
                "score" => 75,
                "result_text" => "Continue providing a holistic understanding of your team's mission and goals and how analytics contribute to achieving them.",
                "question_id" => 23
            ],
            [
                "option" => "C",
                "text" => "Some departments understand the role analytics play in reaching our team/organizational goals.",
                "score" => 50,
                "result_text" => "Ensure that your organization has a holistic understanding of its mission and goals and how analytics contribute to achieving them.",
                "question_id" => 23
            ],
            [
                "option" => "D",
                "text" => "The organization's mission, goals and analytics program is only clearly understood by one or two departments.",
                "score" => 25,
                "result_text" => "Ensure that your organization has a holistic understanding of its mission and goals and how analytics contribute to achieving them.",
                "question_id" => 23
            ],
            [
                "option" => "E",
                "text" => "The team's mission, goals and the role of analytics is not clear to most of the organization.",
                "score" => 0,
                "result_text" => "Ensure that your organization has a holistic understanding of its mission and goals and how analytics contribute to achieving them.",
                "question_id" => 23
            ],

            [
                "option" => "A",
                "text" => "Yes, we drive and capture innovative analytics at every level within the organization.",
                "score" => 100,
                "result_text" => "Keep building systems for driving and capturing innovative metrics.",
                "question_id" => 24
            ],
            [
                "option" => "B",
                "text" => "We're pretty good at encouraging and capturing innovative metrics?",
                "score" => 75,
                "result_text" => "Keep building systems for driving and capturing innovative metrics.",
                "question_id" => 24
            ],
            [
                "option" => "C",
                "text" => "We recognize the value of innovative analytics, but don't have a systematic process for innovation.",
                "score" => 50,
                "result_text" => "Implement a proven system for driving and capturing innovative metrics.",
                "question_id" => 24
            ],
            [
                "option" => "D",
                "text" => "We look at new ideas with analytics, but tend to stick with the status quo.",
                "score" => 25,
                "result_text" => "Implement a proven system for driving and capturing innovative metrics.",
                "question_id" => 24
            ],
            [
                "option" => "E",
                "text" => "We don't put enough time or value on innovative  analytics.",
                "score" => 0,
                "result_text" => "Implement a proven system for driving and capturing innovative metrics.",
                "question_id" => 24
            ],

            [
                "option" => "A",
                "text" => "Yes, we have implemented a proven method for measuring player's social-political impact on our bottom line.",
                "score" => 100,
                "result_text" => "Keep establishing methods for measuring the social and/or political impact players have and the value they create or burn via their social media activities.",
                "question_id" => 25
            ],
            [
                "option" => "B",
                "text" => "Yes, we have a method for measuring player's social-political impact on our bottom line.",
                "score" => 75,
                "result_text" => "Keep establishing methods for measuring the social and/or political impact players have and the value they create or burn via their social media activities.",
                "question_id" => 25
            ],
            [
                "option" => "C",
                "text" => "Yes, we have a method for measuring player's social-political impact on our bottom line, but are still figuring our the algorithms.",
                "score" => 50,
                "result_text" => "Establish a method for measuring the social and/or political impact players have and the value they create or burn via their social media activities.",
                "question_id" => 25
            ],
            [
                "option" => "D",
                "text" => "We have discussed methods for measuring player's social-political impact on our bottom line.",
                "score" => 25,
                "result_text" => "Establish a method for measuring the social and/or political impact players have and the value they create or burn via their social media activities.",
                "question_id" => 25
            ],
            [
                "option" => "E",
                "text" => "We have not begun measuring player's social-political impact.",
                "score" => 0,
                "result_text" => "Establish a method for measuring the social and/or political impact players have and the value they create or burn via their social media activities.",
                "question_id" => 25
            ],
            [
                "option" => "A",
                "text" => "Data drives continuous performance model innovation.",
                "score" => 100,
                "result_text" => "Continue leveraging player-centric and fan-centric insight and driving continuous performance model innovation.",
                "question_id" => 26
            ],
            [
                "option" => "B",
                "text" => "The team's strategy realizes competitive advantage using player-centric and fan-centric insight.",
                "score" => 75,
                "result_text" => "Continue leveraging player-centric and fan-centric insight and driving continuous performance model innovation.",
                "question_id" => 26
            ],
            [
                "option" => "C",
                "text" => "The team's strategy encourages data from new data sources.",
                "score" => 50,
                "result_text" => "Establish a competitive advantage using player-centric and fan-centric insight, and data that drives continuous performance model innovation.",
                "question_id" => 26
            ],
            [
                "option" => "D",
                "text" => "Team recognizes that data can be used to maximize value and ROI, but realization is largely experimental.",
                "score" => 25,
                "result_text" => "Establish a competitive advantage using player-centric and fan-centric insight, and data that drives continuous performance model innovation.",
                "question_id" => 26
            ],
            [
                "option" => "E",
                "text" => "Big data is discussed but not reflected in business strategy.",
                "score" => 0,
                "result_text" => "Establish a competitive advantage using player-centric and fan-centric insight, and data that drives continuous performance model innovation.",
                "question_id" => 26
            ],

            [
                "option" => "A",
                "text" => "Strongly agree.",
                "score" => 100,
                "result_text" => "Continue you habit of taking immediate action and making adjustments based on analysis of team data.",
                "question_id" => 27
            ],
            [
                "option" => "B",
                "text" => "Agree.",
                "score" => 75,
                "result_text" => "Continue you habit of taking immediate action and making adjustments based on analysis of team data.",
                "question_id" => 27
            ],
            [
                "option" => "C",
                "text" => "It's adequate.",
                "score" => 50,
                "result_text" => "Ensure that processes are in place to take immediate action and/or make adjustments based on analysis of team data.",
                "question_id" => 27
            ],
            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Ensure that processes are in place to take immediate action and/or make adjustments based on analysis of team data.",
                "question_id" => 27
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree.",
                "score" => 0,
                "result_text" => "Ensure that processes are in place to take immediate action and/or make adjustments based on analysis of team data.",
                "question_id" => 27
            ],

            [
                "option" => "A",
                "text" => "Yes, we are fully invested in wearable technologies",
                "score" => 100,
                "result_text" => "Keep leveraging wearable technology to transform athletic performance into relevant, digestible and applicable information for coaches and executive management to act upon.",
                "question_id" => 28
            ],
            [
                "option" => "B",
                "text" => "Yes, integrate at least one form of wearable technology into our analytics program.",
                "score" => 75,
                "result_text" => "Keep leveraging wearable technology to transform athletic performance into relevant, digestible and applicable information for coaches and executive management to act upon.",
                "question_id" => 28
            ],
            [
                "option" => "C",
                "text" => "We've tested one or more devices, and are exploring the possibilities.",
                "score" => 50,
                "result_text" => "Keep leveraging wearable technology to transform athletic performance into relevant, digestible and applicable information for coaches and executive management to act upon.",
                "question_id" => 28
            ],
            [
                "option" => "D",
                "text" => "We don't currently invest in wearable technology, but plan to when the market matures.",
                "score" => 25,
                "result_text" => "Keep leveraging wearable technology to transform athletic performance into relevant, digestible and applicable information for coaches and executive management to act upon.",
                "question_id" => 28
            ],
            [
                "option" => "E",
                "text" => "No, we don't invest in wearable technology, and do not plan to in the near future.",
                "score" => 0,
                "result_text" => "Keep leveraging wearable technology to transform athletic performance into relevant, digestible and applicable information for coaches and executive management to act upon.",
                "question_id" => 28
            ],

            [
                "option" => "A",
                "text" => "Yes, we use an integrated ERP system that ties all of our business functions into one platform.",
                "score" => 100,
                "result_text" => "Continue integrating your business and team systems into an integrated platform.",
                "question_id" => 29
            ],
            [
                "option" => "B",
                "text" => "Yes, we use an HRIS system to manage our HR functions and provide a 360 view of employees.",
                "score" => 75,
                "result_text" => "Continue integrating your business and team systems into an integrated platform.",
                "question_id" => 29
            ],
            [
                "option" => "C",
                "text" => "HRIS solutions serve merely as filing cabinets for record keeping with no real business impact.",
                "score" => 50,
                "result_text" => "Integrate and automate Human Resources functions (such as payroll, employee administration, time management, and benefits) into a single platform.",
                "question_id" => 29
            ],
            [
                "option" => "D",
                "text" => "We are looking into systems to integrate our HR data and possibly other business functions.",
                "score" => 25,
                "result_text" => "Integrate and automate Human Resources functions (such as payroll, employee administration, time management, and benefits) into a single platform.",
                "question_id" => 29
            ],
            [
                "option" => "E",
                "text" => "We don't have the funding or staffing to implement an HRIS system.",
                "score" => 0,
                "result_text" => "Integrate and automate Human Resources functions (such as payroll, employee administration, time management, and benefits) into a single platform.",
                "question_id" => 29
            ],

            [
                "option" => "A",
                "text" => "Yes, our media department is fully invested in analytics for real-time consumer engagement.",
                "score" => 100,
                "result_text" => "Continue applying media analytics on the consumers who interact with your digital and social platforms and engaging on the fly.",
                "question_id" => 30
            ],
            [
                "option" => "B",
                "text" => "Yes, our media department uses analytics to understand our consumers better.",
                "score" => 75,
                "result_text" => "Continue applying media analytics on the consumers who interact with your digital and social platforms and engaging on the fly.",
                "question_id" => 30
            ],
            [
                "option" => "C",
                "text" => "Yes, our media department is invested in analytics, but we struggle to quantify the returns.",
                "score" => 50,
                "result_text" => "Apply media analytics on the consumers who interact with your digital and social platforms to improve engagement on the fly.",
                "question_id" => 30
            ],
            [
                "option" => "D",
                "text" => "Sort of. Our media department is beginning to leverage media analytics to engage consumers.",
                "score" => 25,
                "result_text" => "Apply media analytics on the consumers who interact with your digital and social platforms to improve engagement on the fly.",
                "question_id" => 30
            ],
            [
                "option" => "E",
                "text" => "No, we don't apply media analytics to engage consumers.",
                "score" => 0,
                "result_text" => "Apply media analytics on the consumers who interact with your digital and social platforms to improve engagement on the fly.",
                "question_id" => 30
            ],

            [
                "option" => "A",
                "text" => "Yes, our security team is fully invested in analytics for real-time data protection.",
                "score" => 100,
                "result_text" => "Continue applying security analytics to detect and mitigate cyber attacks, and protect sensitive team data.",
                "question_id" => 31
            ],
            [
                "option" => "B",
                "text" => "Our security team uses analytics, but we are still vulnerable ",
                "score" => 75,
                "result_text" => "Continue applying security analytics to detect and mitigate cyber attacks, and protect sensitive team data.",
                "question_id" => 31
            ],
            [
                "option" => "C",
                "text" => "We think we are safe from a cyber attack on our data.",
                "score" => 50,
                "result_text" => "Apply security analytics to detect and mitigate cyber attacks, and protect sensitive team data.",
                "question_id" => 31
            ],
            [
                "option" => "D",
                "text" => "Our focus has been more on physical security, than data security.",
                "score" => 25,
                "result_text" => "Apply security analytics to detect and mitigate cyber attacks, and protect sensitive team data.",
                "question_id" => 31
            ],
            [
                "option" => "E",
                "text" => "Our data has been compromised and we still are not fully secure.",
                "score" => 0,
                "result_text" => "Apply security analytics to detect and mitigate cyber attacks, and protect sensitive team data.",
                "question_id" => 31
            ],

            [
                "option" => "A",
                "text" => "Strongly agree.",
                "score" => 100,
                "result_text" => "Keep implementing an analytics platform that is scalable and can efficiently and economically scale up (or down) as data sources are added/removed.",
                "question_id" => 32
            ],
            [
                "option" => "B",
                "text" => "Agree.",
                "score" => 75,
                "result_text" => "Keep implementing an analytics platform that is scalable and can efficiently and economically scale up (or down) as data sources are added/removed.",
                "question_id" => 32
            ],
            [
                "option" => "C",
                "text" => "It's adequate.",
                "score" => 50,
                "result_text" => "Implement an analytics platform that is scalable and can efficiently and economically scale up (or down) as data sources are added/removed.",
                "question_id" => 32
            ],
            [
                "option" => "D",
                "text" => "Disagree",
                "score" => 25,
                "result_text" => "Implement an analytics platform that is scalable and can efficiently and economically scale up (or down) as data sources are added/removed.",
                "question_id" => 32
            ],
            [
                "option" => "E",
                "text" => "Strongly Disagree.",
                "score" => 0,
                "result_text" => "Implement an analytics platform that is scalable and can efficiently and economically scale up (or down) as data sources are added/removed.",
                "question_id" => 32
            ],

            [
                "option" => "A",
                "text" => "We use a range of approaches that form an analytics ecosystem.",
                "score" => 100,
                "result_text" => "Continue utilizing a range of technologies to form an analytics ecosystem.",
                "question_id" => 33
            ],
            [
                "option" => "B",
                "text" => "We use a range of technologies, including our data warehouse, Hadoop, and others, but they are siloed.",
                "score" => 75,
                "result_text" => "Continue utilizing a range of technologies to form an analytics ecosystem.",
                "question_id" => 33
            ],
            [
                "option" => "C",
                "text" => "We use an analytic platform or appliance",
                "score" => 50,
                "result_text" => "Where possible, utilize a range of technologies, including data warehouse, Hadoop, cloud hosting, data centers and others, to form an analytics ecosystem.",
                "question_id" => 33
            ],
            [
                "option" => "D",
                "text" => "We have a data warehouse or a data mart",
                "score" => 25,
                "result_text" => "Where possible, utilize a range of technologies, including data warehouse, Hadoop, cloud hosting, data centers and others, to form an analytics ecosystem.",
                "question_id" => 33
            ],
            [
                "option" => "E",
                "text" => "We use flat files or spreadsheets.",
                "score" => 0,
                "result_text" => "Where possible, utilize a range of technologies, including data warehouse, Hadoop, cloud hosting, data centers and others, to form an analytics ecosystem.",
                "question_id" => 33
            ],
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Answers::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        foreach ($data as $val) {
            Answers::insert($val);
        }
    }
}
