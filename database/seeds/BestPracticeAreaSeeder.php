<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BestPracticeAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "maturity_area" => "Analytics",
                "best_practice_area" => [
                    //1
                    ["name" => "Team Execution", "recommendation" => "Implement a continuous improvement and adaptation model built around value and analytics in order to gain traction in your division. Implement a system to enable the coaching staff to teach players how to understand and leverage analytics, create value and execute the game plan."],
                    //2
                    ["name" => "Competitive Edge", "recommendation" => "Seek opportunities to leverage analytics to gain a competitive edge."],
                    //3
                    ["name" => "Finance", "recommendation" => "Apply analytics to determine the present value and future value of contracts and to negotiate performance and signing bonuses."],
                    //4
                    ["name" => "Reporting", "recommendation" => "Identify and integrate people, processes and systems that will push your reporting capabilities from reactive and/or siloed reporting to predictive analytics.
                    Establish a competitive advantage using player-centric and fan-centric insight, and data that drives continuous performance model innovation.
                    Apply new analytic trends that help improve performance, predict future outcomes and evaluate the market value of players.
                    Implement data security best practices that ensure your data is accurate and trustworthy.
                    Focus on analyzing data that is meaningful, which the team can take immediate actions to improve performance. 
                    Ensure that processes are in place to take immediate action and/or make adjustments based on analysis of team data.
                    Track player movement to understand how to leverage spacing, distance and speed to gain the greatest competitive edge."],
                ]
            ],
            [
                "maturity_area" => "Data",
                "best_practice_area" => [
                    //5
                    ["name" => "Reporting", "recommendation" => "Identify and integrate people, processes and systems that will push your reporting capabilities from reactive and/or siloed reporting to predictive analytics.
                    Establish a competitive advantage using player-centric and fan-centric insight, and data that drives continuous performance model innovation.
                    Apply new analytic trends that help improve performance, predict future outcomes and evaluate the market value of players.
                    Implement data security best practices that ensure your data is accurate and trustworthy.
                    Focus on analyzing data that is meaningful, which the team can take immediate actions to improve performance. 
                    Ensure that processes are in place to take immediate action and/or make adjustments based on analysis of team data.
                    Track player movement to understand how to leverage spacing, distance and speed to gain the greatest competitive edge."],
                    //6
                    ["name" => "Data Integrity", "recommendation" => "Identify and integrate people, processes and systems that will push your reporting capabilities from reactive and/or siloed reporting to predictive analytics.
                    Establish a competitive advantage using player-centric and fan-centric insight, and data that drives continuous performance model innovation.
                    Apply new analytic trends that help improve performance, predict future outcomes and evaluate the market value of players.
                    Implement data security best practices that ensure your data is accurate and trustworthy.
                    Focus on analyzing data that is meaningful, which the team can take immediate actions to improve performance. 
                    Ensure that processes are in place to take immediate action and/or make adjustments based on analysis of team data.
                    Track player movement to understand how to leverage spacing, distance and speed to gain the greatest competitive edge."],
                ]
            ],
            [
                "maturity_area" => "People",
                "best_practice_area" => [
                    //7
                    ["name" => "Team Identity", "recommendation" => "Define your team’s unique identity and the unique characteristics that will drive you towards your team goals."],
                    //8
                    ["name" => "Fan Diversification", "recommendation" => "Seek opportunities to diversify your fans and sponsorships."],
                    //9
                    ["name" => "Fan Satisfaction", "recommendation" => "Apply analytics to track key fan satisfaction performance indicators and exceed fan expectations."],
                    //10
                    ["name" => "Player Acquisitions/Trades", "recommendation" => "Apply analytics to find, develop and retain quality players that will the teams needs and drive team goals."],
                    //11
                    ["name" => "Senior Management", "recommendation" => "Leverage analytics for senior leadership decision making."],
                    //12
                    ["name" => "Analytics Staff", "recommendation" => "Build a team of data scientists, expert data modelers and statisticians that can generate deep analysis and analytics.
                    Create requirement in your process where analyst must explain results carefully in terms that the team and organization can understand, and clearly demonstrates the progress that has been made."],
                ]
            ],
            [
                "maturity_area" => "Process",
                "best_practice_area" => [
                    //13
                    ["name" => "Team Chemistry", "recommendation" => "Implement a strategy and process to enable the team to play with synergy on offense and defense, utilize spacing effectively and self-organize. Self-organization is a team state that is established when full transparency, inspection, and adaptation are achieved, and the goal of the team is placed above the goals of individuals."],
                    //14
                    ["name" => "Team Execution", "recommendation" => "Implement a continuous improvement and adaptation model built around value and analytics in order to gain traction in your division. Implement a system to enable the coaching staff to teach players how to understand and leverage analytics, create value and execute the game plan."],
                    //15
                    ["name" => "Player IQ", "recommendation" => "Implement processes and training so that players understand the variables and algorithms behind your metrics and exactly how they can add value to reach team goals"]
                ]
            ],
            [
                "maturity_area" => "Profitability",
                "best_practice_area" => [
                    //16
                    ["name" => "Revenue Operations", "recommendation" => "Identify new avenues to create and grow revenues.
                    Utilize sales analytics to understand which fans are more likely to buy, what they are more likely to buy, and when they are likely to buy tickets, merchandise, food and promotions."],
                    //17
                    ["name" => "Recurring Revenues", "recommendation" => "Seek long term contract opportunities to secure future revenues when possible."],
                    //18
                    ["name" => "Sales and Marketing", "recommendation" => "Establish proven and systematic methods for leveraging sales and marketing analytics and gaining sustainable profits."]
                ],
            ],
            ["maturity_area" => "Strategy",
                "best_practice_area" => [
                    //19
                    ["name" => "Mission and Goals", "recommendation" => "Ensure that your organization has a holistic understanding of its mission and goals and how analytics contribute to achieving them."],
                    //20
                    ["name" => "Innovation", "recommendation" => "Implement a proven system for driving in capturing innovative metrics."],
                    //21
                    ["name" => "Social Impact", "recommendation" => "Establish a method for measuring the social and/or political impact players have and the value they create or burn via their social media activities."],
                    //22
                    ["name" => "Data and Reporting", "recommendation" => "Identify and integrate people, processes and systems that will push your reporting capabilities from reactive and/or siloed reporting to predictive analytics.
                    Establish a competitive advantage using player-centric and fan-centric insight, and data that drives continuous performance model innovation.
                    Apply new analytic trends that help improve performance, predict future outcomes and evaluate the market value of players.
                    Implement data security best practices that ensure your data is accurate and trustworthy.
                    Focus on analyzing data that is meaningful, which the team can take immediate actions to improve performance. 
                    Ensure that processes are in place to take immediate action and/or make adjustments based on analysis of team data.
                    Track player movement to understand how to leverage spacing, distance and speed to gain the greatest competitive edge."]
                ]
            ],
            [
                "maturity_area" => "Technology",
                "best_practice_area" => [
                    //23
                    ["name" => "Player Development", "recommendation" => "Leverage wearable technology such as GPS trackers, cameras, heart-rate monitors, and compression suits to transform athletic performance into relevant, digestible and applicable information for coaches and executive management to act upon."],
                    //24
                    ["name" => "Human Resources", "recommendation" => "Integrate and automate Human Resources functions (such as payroll, employee administration, time management, and benefits) into a single platform."],
                    //25
                    ["name" => "Media", "recommendation" => "Apply media analytics on the consumers who interact with your digital and social platforms to improve engagement on the fly."],
                    //26
                    ["name" => "Security", "recommendation" => "Apply security analytics to detect and mitigate cyber attacks, and protect sensitive team data."],
                    //27
                    ["name" => "Analytics Platform", "recommendation" => "Implement an analytics platform that is scalable and can efficiently and economically scale up (or down) as data sources are added/removed."],
                    //28
                    ["name" => "Infrastructure", "recommendation" => "Where possible, utilize a range of technologies, including data warehouse, Hadoop, cloud hosting, data centers and others, to form an analytics ecosystem."]
                ]
            ],
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        (new App\Models\BestPracticeArea)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        foreach ($data as $key => $val) {
            $maturity_id = (new App\Models\MaturityArea)->where('name', '=', $val['maturity_area'])->first()->id;
            $bestPracticeArea = $val['best_practice_area'];
            $inputData = [];
            foreach ($bestPracticeArea as $k => $v) {
                array_push($inputData, ['maturity_area_id' => $maturity_id, 'name' => $v['name'], 'recommendation' => $v['recommendation']]);
            }
            (new App\Models\BestPracticeArea)->insert($inputData);
        }
    }
}
