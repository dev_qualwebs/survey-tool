<?php

use App\Models\Challenges;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChallengesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["challenge" => "Inaccurate, inconsistent, or hard to access data."],
            ["challenge" => "Lack of technical acumen across departments."],
            ["challenge" => "Lack of adequate investment in analytical systems."],
            ["challenge" => "Data does not drive profits and player/team outcomes."],
            ["challenge" => "Inability to measure player value and acquire the best players."],
            ["challenge" => "Team is overwhelmed as a result of big data."],
            ["challenge" => "Translating advanced analytics to the players and coaches."],
            ["challenge" => "Analyzing game data patterns to predict outcomes."],
        ];
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Challenges::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Challenges::insert($data);
    }
}
