<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        $this->call([
            MaturityAreaSeeder::class,
            BestPracticeAreaSeeder::class,
            ChallengesSeeder::class,
            QuestionsSeeder::class,
            AnswersSeeder::class,
            AnalyticMethodsSeeder::class,
            RolesSeeder::class,
            AddAdminUserSeeder::class,
            WelcomeContentSeeder::class
        ]);
    }
}

