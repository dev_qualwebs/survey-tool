<?php

use App\Models\MaturityArea;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaturityAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $maturityArea = [
            ["name" => "Analytics", "definition" => "Mature teams strive to gain analytical insights through automation and optimize decision making through analytics. They produce transformational data and focus on prediction, optimization, and innovation across the organization.", "text" => "Agile Sports Analytics can help you automate data collection, storage, retrieval analysis and reporting to maximize your team and front office performance. Contact Victor Holman at 1-888-861-8733 to discuss how Agile Sports Analytics can help your team leverage its analytics program to gain a competitive edge."],
            ["name" => "Data", "definition" => "Mature teams collect and manage data from multiple sources, both internal and external to the team, including unstructured data, geospatial data, etc. They document and apply data management and ownership policies, and establish analytics governance teams. And they use a range of technologies and techniques including data warehouses, Hadoop, and analytic appliances, data mining and statistical analysis, which form an analytics ecosystem.", "text" => "Agile Sports Analytics can help your team manage all of your data sources, implement data governance policies, and leverage technologies and analytic techniques that will give your team a competitive edge.  Contact Victor Holman at 1-888-861-8733 to discuss how Agile Sports Analytics can help your team leverage data to gain a competitive edge."],
            ["name" => "People", "definition" => "Mature sports teams have players who understand analytics and how they can create value to help team reach goals. Their analytics staff are skilled data architects and analysts who are able to communicate analytics effectively throughout the organization. They understand their supporters and thrive to enhance the fan experience.", "text" => "Agile Sports Analytics can help your players, coaching staff, analytics team and front office leverage analytics to maximize value. Contact Victor Holman at 1-888-861-8733 to discuss how Agile Sports Analytics can help your team leverage its players and staff knowledge to gain a competitive edge."],
            ["name" => "Process", "definition" => "Mature teams leverage formal frameworks to drive performance excellence both on the court and off. Their process includes regular inspection, swift adaptation when necessary and continuous improvements.", "text" => "Agile Sports Analytics can help you identify and implement processes that drive continuous improvement and help your coaches and players plan and execute their game plan. Contact Victor Holman at 1-888-861-8733 to discuss how Agile Sports Analytics can help your team leverage its processes to gain a competitive edge."],
            ["name" => "Profitability", "definition" => "Mature teams leverage analytics to maximize revenue operations and recurring revenues. They have insights into products/services that yield the highest profit margins. They can adjust faster to changing economic conditions and tailor their products and services for a diverse fan and sponsor base.", "text" => "Agile Sports Analytics can help you "],
            ["name" => "Strategy", "definition" => "Mature teams define the very clear team, player and organizational objectives that are measured by structured Key Performance Indicators (KPIs) designed to quantify success or failure. They integrate multiple data sources and actively leverage analytics for transformational change.", "text" => "Agile Sports Analytics can help you define your team’s identity, goals and objectives, and KPIs and build a roadmap for reaching team goals through analytics. Contact Victor Holman at 1-888-861-8733 to discuss how Agile Sports Analytics can help your team build a strategy to help your players and team reach its performance goals."],
            ["name" => "Technology", "definition" => "Mature teams use relevant tools and systems to enable high-quality data collection, intelligent reports, useful visualizations, and advanced analysis such as statistical modeling, predictive analytics, and machine learning.", "text" => "Agile Sports Analytics can help you implement tools and systems that deliver transparency and intelligence into all aspects of your team’s performance. Contact Victor Holman at 1-888-861-8733 to discuss how Agile Sports Analytics can help your team leverage technology to gain a competitive edge."],
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        MaturityArea::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        MaturityArea::insert($maturityArea);
    }
}
