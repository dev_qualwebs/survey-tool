<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            //Analytics
            ["best_practice_area_id" => 1, "question" => "Over the past three years, has your team consistently improved and gained traction in your division, in terms of wins and/or ranking?"],
            ["best_practice_area_id" => 2, "question" => "Does your analytics program give you an advantage over your competitors"],
            ["best_practice_area_id" => 3, "question" => "Finance uses analytics to determine the present value and future value of contracts and to negotiate performance and signing bonuses."],
            ["best_practice_area_id" => 4, "question" => "Your analytics program has been successful in improving performance, predicting future outcomes and evaluating the market value of players."],
            //Data
            ["best_practice_area_id" => 5, "question" => "What type of reporting best describes your team's capabilities?"],
            ["best_practice_area_id" => 6, "question" => "Your data is accurate and trustworthy."],
            ["best_practice_area_id" => 5, "question" => "The data your team/organization collects and analyzes is meaningful (you can act upon it)."],
            ["best_practice_area_id" => 5, "question" => "Your team tracks player movement to understand how to leverage spacing, distance and speed to gain the greatest competitive edge."],
            //People
            ["best_practice_area_id" => 7, "question" => "Does your team have an unique identity with unique characteristics that provide a competitive advantage?"],
            ["best_practice_area_id" => 8, "question" => "Does your team apply analytics to identify and build a diversified fan and sponsorship base?"],
            ["best_practice_area_id" => 9, "question" => "Does your team apply analytics and track key fan satisfaction performance indicators in order to exceed fan expectations?"],
            ["best_practice_area_id" => 10, "question" => "Does your team use analytics to find, develop and retain quality players that fill the team's needs and drive team goals?"],
            ["best_practice_area_id" => 11, "question" => "Does your senior leadership use analytics to make key decisions?"],
            ["best_practice_area_id" => 12, "question" => "Which best describes your analytics staff/team?"],
            ["best_practice_area_id" => 12, "question" => "Analysts explain results carefully in terms that the team and organization can understand, and clearly demonstrates the progress that has been made."],
            //Process
            ["best_practice_area_id" => 13, "question" => "Your team plays with synergy on offense and defense, utilizes spacing effectively and self-organizes."],
            ["best_practice_area_id" => 14, "question" => "Your coach has system to enable the coaching staff to teach players how to understand and leverage analytics, create value and execute the game plan."],
            ["best_practice_area_id" => 15, "question" => "Your players understand the variables and algorithms behind your metrics and exactly how they can add value to reach team goals."],
            //Profitability
            ["best_practice_area_id" => 16, "question" => "Has your team seen consistent revenue growth greater than your league's average?"],
            ["best_practice_area_id" => 17, "question" => "Does your team have long term contracts in place to secure future revenues?"],
            ["best_practice_area_id" => 18, "question" => "Sales and marketing analytics help produce revenues in a proven and systematic way, ensuring profits are sustainable and processes are repeatable?"],
            ["best_practice_area_id" => 16, "question" => "You utilize analytics to understand which fans are more likely to buy, what they are more likely to buy, and when they are likely to buy tickets, merchandise, food, premium seating, promotions, etc."],
            //Strategy
            ["best_practice_area_id" => 19, "question" => "Does your organization have a holistic understanding of its mission and goals and how analytics contribute to achieving them?"],
            ["best_practice_area_id" => 20, "question" => "Does your organization believe that an innovative analytics program is invaluable to creating an ongoing competitive advantage? Does it have a proven system for driving and capturing innovative metrics?"],
            ["best_practice_area_id" => 21, "question" => "You have a method for measuring the social and/or political impact players have and the value they create or burn via their social media activities."],
            ["best_practice_area_id" => 22, "question" => "What statement best describes your team's strategy regarding big data?"],
            ["best_practice_area_id" => 22, "question" => "Actions and adjustments are immediately taken based on analysis of team data"],
            //Technology
            ["best_practice_area_id" => 23, "question" => "Player data is produced by GPS trackers, cameras, heart-rate monitors, compression suits, and/or other wearables and transformed into relevant, digestible and applicable information for coaches and executive management to act upon."],
            ["best_practice_area_id" => 24, "question" => "Human Resources utilize systems and analytics to improve workforce performance."],
            ["best_practice_area_id" => 25, "question" => "Your Media department uses tools to capture enhanced analytics on the consumers who interact with their digital and social platforms to improve engagement on the fly."],
            ["best_practice_area_id" => 26, "question" => "Your security team uses analytics to detect and mitigate cyber attacks and protect sensitive team data."],
            ["best_practice_area_id" => 27, "question" => "Your analytics platform is scalable and can efficiently and economically scale up (or down) as data sources are added/removed."],
            ["best_practice_area_id" => 28, "question" => "What infrastructure technologies do you currently utilize for your analytics efforts"],
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        (new App\Models\Questions)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        (new App\Models\Questions)->insert($data);
    }
}
