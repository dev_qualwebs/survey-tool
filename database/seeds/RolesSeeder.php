<?php

use App\Models\Roles;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["role" => "Data Analyst"],
            ["role" => "Data Architect"],
            ["role" => "Data Scientist"],
            ["role" => "Reporting Developer/Data Visualization Engineer"],
            ["role" => "Analytics Chief/Director of Analytics"],
            ["role" => "Analytics Consultant"],
            ["role" => "Data Modeler"],
            ["role" => "Business Analyst"],
            ["role" => "Project Manager"],
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Roles::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Roles::insert($data);
    }
}








