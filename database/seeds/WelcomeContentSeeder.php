<?php

use App\Models\WelcomeContent;
use Illuminate\Database\Seeder;

class WelcomeContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ["heading_1" => "How Mature is Your Sports Analytics Program?",
            "heading_2" => " “If you aren’t ahead of the sports analytics game… you’re falling behind.” ",
            "bottom_content" => "<div class=\"col-md-12 padding-0\">
                        <div class=\"col-md-12 mt-5 padding-0\">
                            With sports data increasing at a staggering pace, teams looking for a
                            competitive advantage
                            are realizing the benefits that business intelligence (BI), data discovery, and
                            advanced
                            analytics provide.
                        </div>
                        <br>
                        <div class=\"col-md-12 mt-2 padding-0\">
                            Whether your team has evolved its analytics strategies to yield real-time
                            actionable data
                            for
                            making quick decisions, or it still tracks performance using spreadsheets or
                            simple BI
                            reporting
                            tools and dashboards, data analysis plays an essential role these days in
                            performance and
                            winning games.
                        </div>
                         <br>
                        <div class=\"col-md-12 mt-2 padding-0\">
                            Victor Holman, leading business and sports consultant and founder of Agile
                            Sports Analytics,
                            developed the <b>Sports Analytics Maturity Model</b>
                            to help teams determine their analytics maturity when compared with other teams
                            in their
                            competitive level.
                        </div>
                         <br>
                        <div class=\"col-md-12 mt-2 padding-0\">
                            The <b>Sports Analytics Maturity Assessment Tool</b> quickly identifies your
                            teams’
                            strengths,
                            weaknesses and areas for immediate improvement across
                            the <b>7 key maturity areas and 26 best practices</b> that drive sports
                            analytics and team
                            success. The assessment tool provides the big picture of
                            your analytics program, where it needs to go, and where it should concentrate
                            your attention
                            to
                            create more value for your data.
                        </div>
                         <br>
                        <div class=\"col-md-12 mt-2 padding-0\">
                            Complete the <b>online assessment</b> and receive a set of scores indicating
                            your analytics
                            maturity and
                            adoption of the latest best practices in sports analytics. You will be able to
                            compare your
                            score against others and filter results by industry and company size.
                        </div>
                    </div>
                    <div class=\"col-md-12 padding-0\">
                        <h5 class=\"mt-3\">
                            Get a customized Analytics Roadmap based on your assessment and maturity scores.
                        </h5>
                        <h2 class=\"mt-3 mb-3\">Completing the Assessment</h2>
                        <ul class=\"m-0 padding-0\" style=\"padding-left: 15px;\">
                            <li>
                                <div>The assessment will take less than 10 minutes to complete</div>
                            </li>
                            <li>
                                <div>Answer based on your current state, not desired state</div>
                            </li>
                            <li>
                                <div>
                                    Once completed you will receive your initial recommendations, a sports
                                    analytics
                                    framework guide, and a visual representation of your current maturity
                                    footprint.
                                </div>
                            </li>
                            <li>
                                <div>
                                    Be sure to read the guide you receive, which provides a step by step
                                    process for
                                    integrating analytics into your team’s game plan
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class=\"col-md-12 padding-0\">
                        <h2 class=\"mt-3 mb-4\">Privacy & Confidentiality</h2>
                        We value your privacy. Your answers will be shared with Victor Holman, creator of
                        the Sports
                        Analytics Maturity Model and author of ‘Agile Sports Framework - A Blueprint for
                        Increasing
                        Player Value, Improving IQ and Creating Team Synergy’. Aggregated, anonymized
                        results will be
                        used for benchmarking. Authenticating with LinkedIn is mandatory in order to
                        guarantee data
                        quality and simplicity.
                    </div>",
            "image" => "/storage/app/public/default/default.png",
            "home_page_link" => "https://www.agilesportsanalytics.com",
            "sports_analytics_link" => "https://www.agilesportsanalytics.com/sports-analytics-methods-and-processes/",
            "attention_coaches_link" => "https://www.agilesportsanalytics.com/execute-coaching-strategy-and-game-plan/",
            "elit_athlete_link" => "https://www.agilesportsanalytics.com/elite-athlete-academic-counseling/",
            "contact_us_link" => "https://www.agilesportsanalytics.com/contact-victor-holman-sports-team-consultant-agile-sports-expert/",
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        WelcomeContent::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        WelcomeContent::insert($data);
    }
}
