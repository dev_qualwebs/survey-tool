var common_url = 'api/common/';
var user_url = 'api/users/';

Vue.use(VeeValidate);

Vue.component('select2Multiple', {
    props: ['options', 'value'],
    template: '#select2-template',
    mounted: function () {
        var vm = this;
        $(this.$el)
            .select2({
                data: this.options
            })
            .val(this.value)
            .trigger('change')
            .on('change', function () {
                vm.$emit('input', $(this).val())
            })
    },
    watch: {
        value: function (value) {
            if ([...value].sort().join(",") !== [...$(this.$el).val()].sort().join(","))
                $(this.$el).val(value).trigger('change');
        },
        options: function (options) {
            $(this.$el).select2({data: options})
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});


Vue.component('select2lLevel', {
    props: ['options', 'value'],
    template: '#select2-level',
    mounted: function () {
        var vm = this;
        $(this.$el)
            .select2({
                data: this.options
            })
            .val(this.value)
            .trigger('change')
            .on('change', function () {
                vm.$emit('input', $(this).val())
            })
    },
    watch: {
        value: function (value) {
            if ([...value].sort().join(",") !== [...$(this.$el).val()].sort().join(","))
                $(this.$el).val(value).trigger('change');
        },
        options: function (options) {
            $(this.$el).select2({data: options})
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});


new Vue({
    el: '#app',
    data: {
        years: [],
        positions: [],
        challenges: [],
        analyticsMethods: [],
        userRoleStaff: [],
        maturityArea: [],

        maturityQues: [],
        teamProfileObj: {
            organization: '',
            start_year: '',
            sports: [],
            sports_data: [],
            challenges: [],
            user_roles_staff: [],
            analytic_methods: [],
            competition_levels: '',
            user_roles_company: '',
        },
        userTeamProfile: {
            analyticMethods: []
        },
        setMaturityArea: [],
        maturityAreaObj: {
            answer_id: '',
            question_id: ''
        },
        maturityAreaScore: [],
        bestPracticeAreaScores: [],
        ourTeamChallenges: [],
        ourTeamAnalyticMethods: [],
        teamAnalyticScores: [],
        othersAnalyticScores: [],
        overAllMaturityArry: [],
        overAllBestPracticeArry: [],
        ourRolesScores: [],
        chartOption: {
            responsive: true,
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        min: 0,
                        max: 100,
                        fontSize: 12,
                        stepSize: 10,
                    },
                }],
                yAxes: [{
                    gridLines: {
                        display: false
                    },
                }],
            }
        },
        singleBarColor: '#00d9f9',
        doubleBarColor: '#ccc',
        sports_data: [
            {name: 'Basketball', id: 'Basketball', text: 'Basketball'},
            {name: 'Baseball', id: 'Baseball', text: 'Baseball'},
            {name: 'Football', id: 'Football', text: 'Football'},
            {name: 'Hockey', id: 'Hockey', text: 'Hockey'},
            {name: 'Lacrosse', id: 'Lacrosse', text: 'Lacrosse'},
            {name: 'Field Hockey', id: 'Field Hockey', text: 'Field Hockey'},
            {name: 'Volleyball', id: 'Volleyball', text: 'Volleyball'},
            {name: 'Softball', id: 'Softball', text: 'Softball'},
        ],
        competition_levels_data: [
            {name: 'Professional', id: 'Professional', text: 'Professional'},
            {name: 'Semi Pro', id: 'Semi Pro', text: 'Semi Pro'},
            {name: 'College', id: 'College', text: 'College'},
            {name: 'High School', id: 'High School', text: 'High School'},
            {name: 'Other', id: 'Other', text: 'Other'},
        ],
        answer: '',
        overAllScore: '',
        overAllScoreData: [],
        tooltipText: '',
        attr: '',
        tabClass: '',
        welcomecontent: {
            id: '',
            image: '',
            heading_1: '',
            heading_2: '',
            bottom_content: '',
            home_page_link: '',
            sports_analytics_link: '',
            attention_coaches_link: '',
            elit_athlete_link: '',
            contact_us_link: '',
        },
        assesmentTab: '',
        welcomeTab: '',
        filePath: ''
    },
    created() {

        var that = this;

        for (var i = 2000; i <= new Date().getFullYear(); i++) {
            that.years.push({'year': i});
        }

        axios.get(common_url + 'challenges').then(function (res) {
            that.challenges = res.data.response;
        });

        axios.get(common_url + 'get-analytic-methods').then(function (res) {
            that.analyticsMethods = res.data.response;
        });

        axios.get(common_url + 'get-maturity-areas').then(function (res) {
            that.maturityArea = res.data.response;
        });

        axios.get(common_url + 'roles').then(function (res) {
            that.userRoleStaff = res.data.response;
        });

        axios.get(common_url + 'get-welcome-content').then(function (res) {
            that.welcomecontent = res.data.response;
            that.welcomecontent.show = true;
        });

        that.user_roles_company = [
            {id: '1', role: 'Team Executive'},
            {id: '2', role: 'Coaching Staff'},
            {id: '3', role: 'Analyst'},
            {id: '4', role: 'Data scientist'},
            {id: '5', role: 'Other'}
        ];

        that.getTeamProfile();

        that.getUserPositions();

        that.getOverAllScore();

        that.getMaturiyAreaScore();

        that.tabClass = document.getElementsByClassName('nav-link');
        for (var c = 0; c < that.tabClass.length; c++) {
            that.attr = that.tabClass[c];
            if (that.attr.classList[1] == 'active') {

            }
            else {
                that.tooltipText = 'You can only jump to next/previous section after answering this section and hitting next/previous button';
            }
        }

        /*Code to display popup on tab/browser close*/
        /*that.assesmentTab = document.getElementById('v-pills-view-assess-tab').getAttribute('assesment');
        that.welcomeTab = document.getElementById('v-pills-welcome-tab').getAttribute('welcome');

        window.onbeforeunload = function () {
            if ((that.assesmentTab === 'yes') && (that.welcomeTab === 'yes')) {
                return "Do you really want to leave our application?";
            }
            else {

            }
        };*/
    },
    methods: {
        getMaturityQuestions: function (name) {
            var this_2 = this;
            this_2.maturityQues = [];
            axios.get(common_url + 'get-maturity-questions?name=' + name).then(function (response) {
                this_2.maturityQues = response.data.response[0].best_practice_area;
                this_2.maturityQues.forEach(function (v, k) {
                    v.questions.forEach(function (val, key) {
                        val.options.forEach(function (value, index) {
                            if (value.id == val.selected.answer_id) {
                                this_2.setMaturityArea[value.question_id] = val.selected.answer_id;
                            }
                        });
                    })
                });
            });
        },
        changeTab: function (remove, add, name) {
            console.log(remove, add);
            var this_3 = this;

            $('#' + remove).removeClass('show active');
            $('#' + add).addClass('show active');
            $('#' + remove + '-tab').removeClass('active');
            $('#' + add + '-tab').addClass('active');

            $("html, body").animate({
                scrollTop: $('html').offset().top + 0
            }, 200);
            this_3.getTeamProfile();

            /*Code to display popup on tab/browser close*/
            if ((remove == 'v-pills-profatibility') && (add == 'v-pills-view-assess')) {
                this_3.getOverAllScore();
                // this_3.assesmentTab = document.getElementById('v-pills-view-assess-tab').setAttribute('assesment', 'no');
            }
            /* else if ((remove == 'v-pills-view-assess') && (add == 'v-pills-profatibility')) {
                 var assesId = document.getElementById('v-pills-view-assess-tab')
                 assesId.setAttribute('assesment', 'yes');
                 this_3.assesmentTab = assesId.getAttribute('assesment');
             }
             else if ((remove == 'v-pills-team-profile') && (add == 'v-pills-welcome')) {
                 this_3.welcomeTab = document.getElementById('v-pills-welcome-tab').setAttribute('welcome', 'no');
             }
             else if ((remove == 'v-pills-welcome') && (add == 'v-pills-team-profile')) {
                 var welcomeId = document.getElementById('v-pills-welcome-tab')
                 welcomeId.setAttribute('welcome', 'yes');
                 this_3.welcomeTab = welcomeId.getAttribute('welcome');
             }*/
            /*Code to display popup on tab/browser close*/

            else if (name) {
                this_3.getMaturityQuestions(name);
            }
        },
        getTeamProfile: function () {
            var that = this;
            axios.get(user_url + 'get-team-profile').then(function (response) {
                var teamProfile = response.data.response;
                if (teamProfile.hasOwnProperty('start_year')) {
                    that.teamProfileObj.start_year = teamProfile.start_year;
                }
                if (teamProfile.hasOwnProperty('organization')) {
                    that.teamProfileObj.organization = teamProfile.organization;
                }
                if (teamProfile.hasOwnProperty('sports')) {
                    var selectedSports = [];
                    selectedSports = teamProfile.sports.split(',');
                    that.sports_data.forEach(function (v, k) {
                        if (selectedSports.indexOf(v.text) != -1) {
                            that.teamProfileObj.sports_data.push(v.text);
                        }
                    });
                }
                if (teamProfile.hasOwnProperty('competition_levels')) {
                    that.teamProfileObj.competition_levels = teamProfile.competition_levels;
                }
                if (teamProfile.hasOwnProperty('user_roles_company') && that.user_roles_company.length) {
                    that.teamProfileObj.user_roles_company = teamProfile.user_roles_company;
                }
                if (teamProfile.hasOwnProperty('challenges') && that.challenges.length) {
                    var userChallenges = teamProfile.challenges.map(obj => {
                        return obj.challenge_id;
                    });
                    userChallenges = JSON.stringify(userChallenges);
                    that.challenges.forEach(function (val, key) {
                        if (userChallenges.indexOf(val.id) != -1) {
                            that.teamProfileObj.challenges[key] = val.id;
                        }
                    })
                }
                if (teamProfile.hasOwnProperty('methods') && that.analyticsMethods.length) {
                    var methodsArray = teamProfile.methods.map(obj => {
                        return obj.analytic_methods_id;
                    });
                    methodsArray = JSON.stringify(methodsArray);
                    that.userTeamProfile.analyticMethods = [];
                    that.analyticsMethods.forEach(function (val, key) {
                        if (methodsArray.indexOf(val.id) != -1) {
                            that.userTeamProfile.analyticMethods.push(val);
                        }
                    });
                    var temp = [];
                    that.analyticsMethods.map(obj => {
                        if (methodsArray.indexOf(obj.id) == -1) {
                            temp.push(obj);
                        }
                    });
                    that.analyticsMethods = temp;
                }
                if (teamProfile.hasOwnProperty('roles') && that.userRoleStaff.length) {
                    var rolesArray = teamProfile.roles.map(obj => {
                        return obj.role_id;
                    });
                    rolesArray = JSON.stringify(rolesArray);
                    that.userRoleStaff.forEach(function (val, key) {
                        if (rolesArray.indexOf(val.id) != -1) {
                            that.teamProfileObj.user_roles_staff[key] = val.id;
                        }
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        },

        getUserPositions: function () {
            var that = this;
            axios.get(user_url + 'get-user-positions').then(function (response) {
                that.positions = response.data.positions;
            });
        },

        updateTeamProfile: function (remove, add, name) {
            var this_4 = this;
            var dropData = document.getElementById('dropadatavalue').childNodes;
            for (n = 0; n < dropData.length; n++) {
                this.teamProfileObj.analytic_methods.push(dropData[n].getAttribute("method"));
            }
            this.teamProfileObj.sports = this.teamProfileObj.sports_data.toString();
            axios.post(user_url + 'update-team-profile', this.teamProfileObj).then(function (response) {
                this_4.changeTab(remove, add);
                this_4.getMaturityQuestions(name);
                this_4.getTeamProfile();
            }).catch(function (error) {
                swal({
                    title: 'Error!',
                    text: error.response.data.message,
                    type: 'error',
                })
            });
        },

        updateMaturityAnswer: function (ques, ans) {
            this.maturityAreaObj.question_id = ques;
            this.maturityAreaObj.answer_id = ans;
            axios.post(user_url + 'update-maturity-answer', this.maturityAreaObj).then(function (response) {

            }).catch(function (error) {
                swal({
                    title: 'Error!',
                    text: error.response.data.message,
                    type: 'error',
                })
            });
        },

        validateForm: function (remove, add, name) {
            var this_5 = this;
            this.$validator.validateAll().then((result) => {
                if (result) {
                    this_5.changeTab(remove, add, name);
                    return;
                }
                swal({
                    title: 'Error!',
                    text: 'Please select all questions.',
                    type: 'error',
                })
            });
        },

        getOverAllScore: function () {
            var that = this;
            axios.get(user_url + 'overall-maturity-score').then(function (response) {
                that.overAllScore = response.data.score;
                that.overAllScoreData = response.data.data;
                var rangeShow = document.querySelector("#show");
                var rangeClock = document.querySelector(".meter-clock");
                var rotateClock = that.overAllScore;
                rangeClock.style.transform = "rotate(" + (-90 + rotateClock * 180 / 100) + "deg)";
                rangeShow.value = rotateClock + "%";
            });
        },

        getMaturiyAreaScore: function () {

            var that = this;

            axios.get(user_url + 'maturity-area-score').then(function (response) {

                that.teamAnalyticScores = response.data.response;
                that.othersAnalyticScores = response.data.others;

                that.overAllMaturityArry = [];

                setTimeout(function () {
                    for (var i = 0; i < that.teamAnalyticScores.length; i++) {

                        var rangeShow = document.getElementById(that.teamAnalyticScores[i].maturity);
                        var rangeClock = document.getElementsByClassName("meter-clock")[i + 1];
                        var rotateClock = that.teamAnalyticScores[i].average;
                        rangeClock.style.transform = "rotate(" + (-90 + rotateClock * 180 / 100) + "deg)";
                        rangeShow.value = rotateClock + "%";

                        that.overAllMaturityArry.push({
                            average: that.teamAnalyticScores[i].average,
                            maturity: that.teamAnalyticScores[i].maturity,
                            Avg: (response.data.others.find(e => e.maturity === that.teamAnalyticScores[i].maturity)).average
                        });

                        var maturityId = document.getElementById("maturityAreaComparison");

                        var dataSet = [];
                        var dataSet_1 = [];
                        var dataChlng = [];
                        that.overAllMaturityArry.forEach(function (el, key) {
                            dataSet.push(el.average);
                            dataSet_1.push(el.Avg);
                            dataChlng.push(el.maturity);
                        });

                        var horizontalBar = new Chart(maturityId, {
                            type: 'horizontalBar',
                            data: {
                                labels: dataChlng,
                                datasets: [{
                                    label: 'Your Team',
                                    data: dataSet,
                                    backgroundColor: that.singleBarColor
                                }, {
                                    label: 'Others',
                                    data: dataSet_1,
                                    backgroundColor: that.doubleBarColor
                                }]
                            },
                            options: that.chartOption
                        });

                    }
                }, 1000);

            });
        },
        getBestPracticesAreaScore: function () {
            var this_6 = this;


            axios.get(user_url + 'best-practice-area-score').then(function (response) {
                this_6.bestPracticeAreaScores = response.data.response;
                var dataSet = [];
                var dataChlng = [];

                this_6.overAllBestPracticeArry = [];

                setTimeout(function () {
                    for (var j = 0; j < this_6.bestPracticeAreaScores.length; j++) {
                        var rangeShow = document.getElementById(this_6.bestPracticeAreaScores[j].best_practice_area);
                        var rangeClock = document.getElementsByClassName("meter-clock_1")[j];
                        var rotateClock = this_6.bestPracticeAreaScores[j].average;
                        rangeClock.style.transform = "rotate(" + (-90 + rotateClock * 180 / 100) + "deg)";
                        rangeShow.value = rotateClock + "%";

                        this_6.overAllBestPracticeArry.push({
                            average: this_6.bestPracticeAreaScores[j].average,
                            best_practice_area: this_6.bestPracticeAreaScores[j].best_practice_area,
                            maturity_area: this_6.bestPracticeAreaScores[j].maturity_area,
                            Avg: (response.data.others.find(e => e.maturity_area === this_6.bestPracticeAreaScores[j].maturity_area)).average
                        });

                        dataSet.push(this_6.bestPracticeAreaScores[j].average);
                        dataChlng.push(this_6.bestPracticeAreaScores[j].best_practice_area);

                    }
                }, 1000)

                var ctx = document.getElementById("practiceAreaComparison");

                var dataSet_1 = [];
                var dataSet_2 = [];
                var dataChlng_2 = [];
                this_6.overAllBestPracticeArry.forEach(function (el, key) {
                    dataSet_1.push(el.average);
                    dataSet_2.push(el.Avg);
                    dataChlng_2.push(el.best_practice_area);
                });

                var horizontalBar_1 = new Chart(ctx, {
                    type: 'horizontalBar',
                    data: {
                        labels: dataChlng_2,
                        datasets: [{
                            label: 'Your Team',
                            data: dataSet_1,
                            backgroundColor: this_6.singleBarColor,
                        }, {
                            label: 'Others',
                            data: dataSet_2,
                            backgroundColor: this_6.doubleBarColor,
                        }]
                    },
                    options: this_6.chartOption
                });

            });
        },
        getOtherTeamChallenges: function () {
            var this_7 = this;
            axios.get(user_url + 'other-team-challenges').then(function (response) {
                this_7.ourTeamChallenges = response.data.challenges;

                var teamId = document.getElementById("otherTeamChlng");
                var dataSet = [];
                var dataChlng = [];

                for (var j = 0; j < response.data.response.length; j++) {
                    dataSet.push(response.data.response[j].count);
                    dataChlng.push(response.data.response[j].challenge);
                }

                var horizontalBar = new Chart(teamId, {
                    type: 'horizontalBar',
                    data: {
                        labels: dataChlng,
                        datasets: [{
                            label: "Challenges",
                            backgroundColor: this_7.singleBarColor,
                            hoverBackgroundColor: this_7.singleBarColor,
                            data: dataSet
                        }]
                    },
                    options: this_7.chartOption
                });
            });
        },
        getOtherTeamAnalyticChallenges: function () {
            var this_8 = this;
            axios.get(user_url + 'other-team-analytic-methods').then(function (response) {
                this_8.ourTeamAnalyticMethods = response.data.methods;

                var ctx = document.getElementById("otherTeamAnaChlng");

                var dataSet = [];
                var dataChlng = [];

                for (var j = 0; j < response.data.response.length; j++) {
                    dataSet.push(response.data.response[j].count);
                    dataChlng.push(response.data.response[j].name);
                }

                var data = {
                    labels: dataChlng,
                    datasets: [{
                        label: "Analytic Methods",
                        backgroundColor: this_8.singleBarColor,
                        hoverBackgroundColor: this_8.singleBarColor,
                        data: dataSet
                    }]
                };

                var horizontalBar = new Chart(ctx, {
                    type: 'horizontalBar',
                    data: data,
                    options: this_8.chartOption
                });
            });
        },
        getRolesBar: function () {
            var this_10 = this;
            axios.get(user_url + 'other-users-roles').then(function (response) {
                this_10.ourRolesScores = response.data.user_roles;

                var ctx = document.getElementById("rolesChallenges");

                var dataSet = [];
                var dataChlng = [];

                for (var j = 0; j < response.data.response.length; j++) {
                    dataSet.push(response.data.response[j].count);
                    dataChlng.push(response.data.response[j].role);
                }

                var data = {
                    labels: dataChlng,
                    datasets: [{
                        label: "Roles",
                        backgroundColor: this_10.singleBarColor,
                        hoverBackgroundColor: this_10.singleBarColor,
                        data: dataSet
                    }]
                };


                var horizontalBar = new Chart(ctx, {
                    type: 'horizontalBar',
                    data: data,
                    options: this_10.chartOption
                });
            });
        },
        login: function () {
            window.open(('/survey-tool/login/linkedin'), "SignIn", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left=" + 250 + ",top=" + 150);
        },
        generatePdf: function () {
            var that = this;
            axios.get(user_url + 'generate-pdf').then(function (res) {
                that.filePath = res.data.url;
                setTimeout(function () {
                    document.getElementById('exportAssesmentPdf').click();
                }, 500);

            });
        }
    }
});

document.addEventListener("dragstart", function (event) {
    event.dataTransfer.setData("Text", event.target.id);
});

document.addEventListener("dragover", function (event) {
    event.preventDefault();
});

document.addEventListener("drop", function (event) {
    event.preventDefault();
    if (event.target.className == "col-md-5 drop-box") {
        var data = event.dataTransfer.getData("Text");
        event.target.appendChild(document.getElementById(data));
    }
});

$('#select-sports').select2({
    tags: true,
    tokenSeparators: [',', ' ']
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
