@extends('admin.app')

@section('content')
    <div class="col-md-12 p-0">
        <div class="d-flex flex-column bd-highlight mb-3">
            <h4 class="mt-3">Add Question
                <a class="pull-right link ml-2" style="font-size: 17px;" href="{{ url('admin/questions') }}">
                    <i class="material-icons">undo</i>Back
                </a>
            </h4>
            <form @submit.prevent="validateForm" action="{{url('/')}}/admin/post-add-question" method="post" class="">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label for="exampleInputEmail1">Maturity Area</label>
                    <select class="form-control" onchange="getBPArea(this.value)" name="maturity_area" v-validate="'required'">
                        @foreach($mareas as $key => $val)
                            <option value="{{$val->id}}">{{$val->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Best Practice Area</label>
                    <select class="form-control" id="bp_area" name="bp_area" v-validate="'required'">

                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Question</label>
                    <input type="text" class="form-control" name="question" placeholder="Question" v-validate="'required'">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Option A</label>
                    <input type="text" class="form-control" name="option_a" placeholder="Option A" v-validate="'required'">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Result for option A</label>
                    <input type="text" class="form-control" name="result_text_a" placeholder="Result A" v-validate="'required'">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Option B</label>
                    <input type="text" class="form-control" name="option_b" placeholder="Option B" v-validate="'required'">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Result for option B</label>
                    <input type="text" class="form-control" name="result_text_b" placeholder="Result B" v-validate="'required'">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Option C</label>
                    <input type="text" class="form-control" name="option_c" placeholder="Option C" v-validate="'required'">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Result for option C</label>
                    <input type="text" class="form-control" name="result_text_c" placeholder="Result C" v-validate="'required'">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Option D</label>
                    <input type="text" class="form-control" name="option_d" placeholder="Option D" v-validate="'required'">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Result for option D</label>
                    <input type="text" class="form-control" name="result_text_d" placeholder="Result D" v-validate="'required'">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Option E</label>
                    <input type="text" class="form-control" name="option_e" placeholder="Option E" v-validate="'required'">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Result for option E</label>
                    <input type="text" class="form-control" name="result_text_e" placeholder="Result E" v-validate="'required'">
                </div>
                <button name="submit" value="Submit" class="btn btn-info pull-right" type="submit">Submit</button>
            </form>
        </div>
    </div>
@endsection
