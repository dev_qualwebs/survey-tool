<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="{{ asset('node_modules/font-awesome/css/font-awesome.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('node_modules/daemonite-material/css/material.min.css')}}">
    <link type="text/css" rel="stylesheet"
          href="{{ asset('node_modules/bootstrap-multiselect/dist/css/bootstrap-multiselect.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('node_modules/sweetalert2/dist/sweetalert2.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('node_modules/sweetalert2/dist/sweetalert2.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/admin-custom.css')}}">
    <!-- Fonts -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Pacifico|Questrial">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,600">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="shortcut icon" href="{{ asset('public/images/asa.jpg') }}">
</head>
<body>
<div id="adminApp">
    <div class="container-fluid">
        <div id="main" class="row">
            <div class="col-md-12 main">
                <div class="row">
                    <div class="col-md-3 left-content p-0">
                        <div class="sticky">
                            <div class="col-md-12 top-logo text-center">
                                <img width="200px" src="{{ asset('public/images/main-log-site.png') }}">
                            </div>
                            <div class="col-md-12 side-links px-4">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                     aria-orientation="vertical">
                                    <a class="nav-link {{ Request::is('admin') ? 'active' : ''}}"
                                       href="{{ url('admin') }}">
                                        <span class="count">1</span>Dashboard
                                    </a>
                                    <a class="nav-link {{ (Request::is('admin/maturity-areas') || Request::is('admin/add-maturity-areas') ||  Request::is('admin/edit-maturity-areas')) ? 'active' : '' }}"
                                       href="{{ url('admin/maturity-areas') }}">
                                        <span class="count">2</span>Maturity Areas
                                    </a>
                                    <a class="nav-link {{ Request::is('admin/best-practices') ? 'active' : '' }}"
                                       href="{{ url('admin/best-practices') }}">
                                        <span class="count">3</span>Best Practices
                                    </a>
                                    <a class="nav-link {{ Request::is('admin/questions') ? 'active' : '' }}"
                                       href="{{ url('admin/questions') }}">
                                        <span class="count">4</span>Questions
                                    </a>
                                    <a class="nav-link {{ Request::is('admin/users') ? 'active' : '' }}"
                                       href="{{ url('admin/users') }}">
                                        <span class="count">5</span>Users
                                    </a>
                                    <a class="nav-link {{ Request::is('admin/welcome-content') ? 'active' : '' }}"
                                       href="{{ url('admin/welcome-content') }}">
                                        <span class="count">6</span>CMS
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 right-content p-0">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                                    <li class="nav-item">
                                        <a class="nav-link logout" href="{{ route('admin/logout') }}"
                                           onclick="event.preventDefault();document.getElementById('admin-logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="admin-logout-form" action="{{ route('admin/logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="col-md-12 tab-views" style="max-width: 98% !important;">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <script type="text/javascript" src="{{ asset('node_modules/jquery/dist/jquery.js')}}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/sweetalert2/dist/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/daemonite-material/js/material.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/vue/dist/vue.js')}}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/axios/dist/axios.js')}}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/vee-validate/dist/vee-validate.js')}}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript"
            src="{{ asset('node_modules/@tinymce/tinymce-vue/lib/browser/tinymce-vue.js')}}"></script>
    <script type="text/javascript" src="{{ asset('node_modules/chart.js/dist/Chart.min.js')}}"></script>
</footer>
<script>

    var url = '{{url('/')}}';
    var admin_url = '{{url('/')}}/api/admin/';

    function getBPArea(mid) {
        $.get('{{url('/')}}/api/admin/get-bp-area/' + mid, function (data) {
            var str = '';
            $.each(data.response, function (key, val) {
                str += '<option value="' + val.id + '">' + val.name + '</option>'
            });
            $('#bp_area').html(str);
        });
    }

    jQuery('document').ready(function () {
        getBPArea(1);
    });

    Vue.use(VeeValidate);

    new Vue({
        el: '#adminApp',
        data: {
            questionsDetails: [],
            questionsUpdateObj: {
                answers: [],
                question: '',
                question_id: ''
            },
            ques: '',
            practiceAreaObj: {
                name: '',
                recommendation: '',
                maturity_area_id: ''
            },
            maturityAreaObj: {
                name: '',
                definition: '',
                maturity_text: ''
            },
            singleMaturityObj: {
                id: '',
                name: '',
                maturity: '',
                definition: '',
                maturity_text: ''
            },
            singlePracticeObj: {
                name: '',
                practice: '',
                practice_id: '',
                recommendation: ''
            },
            userdetailObj: {},
            userMaturityScore: [],
            dashboardDetails: [],
            chartOption: {
                responsive: true,
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            min: 0,
                            max: 100,
                            fontSize: 12,
                            stepSize: 10,
                        },
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false
                        },
                    }],
                }
            },
            singleBarColor: '#00d9f9',
            doubleBarColor: '#ccc',
            welcomecontent: {
                heading_1: '',
                heading_2: '',
                bottom_content: '',
                image: '',
                home_page_link: '',
                sports_analytics_link: '',
                attention_coaches_link: '',
                elit_athlete_link: '',
                contact_us_link: '',
            },
            imagPath: ''
        },
        created() {
            this.getQuestionOption();
            this.singleMaturityArea();
            this.singleBestPracticeArea();
            this.getUserProfile();
            this.getWelcomeContent();

            var that = this;
            var url = document.URL;
            var urlSplit = url.split("/");

            axios.post(admin_url + 'user-maturity-score', {'user_id': urlSplit[urlSplit.length - 1]}).then(function (res) {
                that.userMaturityScore = res.data;
            });

            axios.get(admin_url + 'users-graph').then(function (res) {
                that.dashboardDetails = res.data;

                var maturityId = document.getElementById("maturityAdmin");
                var practiceId = document.getElementById("bestPracticeAdmin");

                var dataSet = [], dataChlng = [], dataSet_1 = [], dataChlng_1 = [];

                for (var i = 0; i < that.dashboardDetails.maturity.length; i++) {
                    dataSet.push(that.dashboardDetails.maturity[i].average);
                    dataChlng.push(that.dashboardDetails.maturity[i].maturity);
                }

                for (var j = 0; j < that.dashboardDetails.best_practice.length; j++) {
                    dataSet_1.push(that.dashboardDetails.best_practice[j].average);
                    dataChlng_1.push(that.dashboardDetails.best_practice[j].best_practice_area);
                }

                var horizontalBar = new Chart(maturityId, {
                    type: 'horizontalBar',
                    data: {
                        labels: dataChlng,
                        datasets: [{
                            label: "Maturity",
                            backgroundColor: that.singleBarColor,
                            hoverBackgroundColor: that.singleBarColor,
                            data: dataSet
                        }]
                    },
                    options: that.chartOption
                });

                var horizontalBar_1 = new Chart(practiceId, {
                    type: 'horizontalBar',
                    data: {
                        labels: dataChlng_1,
                        datasets: [{
                            label: "Best Practice",
                            backgroundColor: that.singleBarColor,
                            hoverBackgroundColor: that.singleBarColor,
                            data: dataSet_1
                        }]
                    },
                    options: that.chartOption
                });


            });

        },
        components: {
            'editor': Editor // <- Important part
        },
        methods: {
            checkEdit: function (val) {
                if (val == 'show') {
                    $('#editForm').css('display', 'block');
                    $('#viewFrom').css('display', 'none');
                }
                else if (val == 'hide') {
                    $('#editForm').css('display', 'none');
                    $('#viewFrom').css('display', 'block');
                }
            },
            editQuestionDetails: function () {
                var that = this;
                var url = document.URL;
                var urlSplit = url.split("/");
                that.questionsUpdateObj.question_id = urlSplit[urlSplit.length - 1];
                axios.post(admin_url + 'edit-question', this.questionsUpdateObj).then(function (res) {
                    $('#editForm').css('display', 'none');
                    $('#viewFrom').css('display', 'block');
                    that.getQuestionOption();
                });
            },
            getQuestionOption: function () {
                var that = this;
                var url = document.URL;
                var urlSplit = url.split("/");
                axios.get(admin_url + 'get-question-options/' + urlSplit[urlSplit.length - 1]).then(function (res) {
                    that.questionsDetails = res.data.response;
                    that.questionsUpdateObj.answers = res.data.response;
                    that.ques = res.data.ques;
                    that.questionsUpdateObj.question = res.data.ques.question;
                });
            },
            addBestPracticeArea: function () {
                axios.post(admin_url + 'add-practice-area', this.practiceAreaObj).then(function (res) {
                    window.location.href = '{{url('/')}}/admin/best-practices'
                });
            },
            addMturityArea: function () {
                axios.post(admin_url + 'post-maturity-area', this.maturityAreaObj).then(function (res) {
                    window.location.href = '{{url('/')}}/admin/maturity-areas';
                });
            },
            singleMaturityArea: function () {
                var that = this;
                var url = document.URL;
                var urlSplit = url.split("/");
                axios.post(admin_url + 'single-maturity-area', {'maturity_id': urlSplit[urlSplit.length - 1]}).then(function (res) {
                    that.singleMaturityObj = res.data.response;
                    that.singleMaturityObj.maturity = res.data.response.name;
                });
            },
            editSingleMaturity: function () {
                var that = this;
                var url = document.URL;
                var urlSplit = url.split("/");
                that.singleMaturityObj.id = urlSplit[urlSplit.length - 1];
                axios.post(admin_url + 'edit-single-maturity', this.singleMaturityObj).then(function (res) {
                    window.location.href = '{{url('/')}}/admin/maturity-areas';
                });
            },
            singleBestPracticeArea: function () {
                var that = this;
                var url = document.URL;
                var urlSplit = url.split("/");
                axios.post(admin_url + 'single-best-practice-area', {'practice_id': urlSplit[urlSplit.length - 1]}).then(function (res) {
                    that.singlePracticeObj = res.data.response;
                    that.singlePracticeObj.practice = res.data.response.name;
                });
            },
            editSinglePracticeArea: function () {
                var that = this;
                var url = document.URL;
                var urlSplit = url.split("/");
                that.singlePracticeObj.practice_id = urlSplit[urlSplit.length - 1];
                axios.post(admin_url + 'edit-single-best-practice-area', this.singlePracticeObj).then(function (res) {
                    window.location.href = '{{url('/')}}/admin/best-practices';
                });
            },
            getUserProfile: function () {
                var that = this;
                var url = document.URL;
                var urlSplit = url.split("/");
                axios.post(admin_url + 'get-user-profile', {'user_id': urlSplit[urlSplit.length - 1]}).then(function (res) {
                    that.userdetailObj = res.data.response;
                });
            },
            validateForm: function (remove, add, name) {
                var this_5 = this;
                this.$validator.validateAll().then((result) => {
                    if (result) {

                    }
                    swal({
                        title: 'Error!',
                        text: 'Please select all questions.',
                        type: 'error',
                    })
                });
            },
            getWelcomeContent: function () {
                var that = this;
                axios.get(admin_url + 'welcome-content').then(function (res) {
                    that.welcomecontent = res.data.response;
                    setTimeout(function () {
                        document.getElementsByTagName('iframe')[0].style.height = '650px';
                        document.getElementsByClassName('mce-widget mce-notification mce-notification-warning mce-has-close mce-in')[0].style.display = 'none';
                    }, 500);
                });
            },
            updateWelcomeContent: function () {
                var that = this;
                axios.post(admin_url + 'update-welcome-content', this.welcomecontent).then(function (res) {
                    that.getWelcomeContent();
                    swal({
                        title: 'Success!',
                        text: res.data.response,
                        type: 'success',
                    })
                });
            },
            uploadImg: function (file) {
                var that = this;
                var file = $('#image')[0].files[0];
                var fd = new FormData();
                fd.append('file', file);
                axios.post(admin_url + 'upload-image', fd).then(function (res) {
                    that.welcomecontent.image = '{{url('/')}}' + res.data.path;
                    swal({
                        title: 'Success!',
                        text: 'Image Uploaded successfully !',
                        type: 'success',
                    })
                }).catch(function (error) {
                    swal({
                        title: 'Error!',
                        text: error.data.message,
                        type: 'error',
                    })
                });
            }
        }

    });

    Vue.filter('userRoles', function (value) {
        if (value == 1) {
            return 'Team Executive'
        }
        if (value == 2) {
            return 'Coaching Staff';
        }
        if (value == 3) {
            return 'Analyst';
        }
        if (value == 4) {
            return 'Data scientist';
        }
        if (value == 5) {
            return 'Other';
        }
    })
</script>
</body>
</html>
