@extends('admin.app')

@section('content')
    <div class="d-flex flex-column bd-highlight mb-3">
        <div class="col-md-12 p-0">
            <h4 class="mt-3">Edit @{{singlePracticeObj.practice}} Details
                <a class="pull-right link ml-2" style="font-size: 17px;" href="{{ url('admin/best-practices') }}">
                    <i class="material-icons">undo</i>Back
                </a>
            </h4>
            <form>
                <div class="col-md-12 p-0 mt-2">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Maturity Area</label>
                        <select class="form-control" name="maturityArea" v-model="singlePracticeObj.maturity_area_id">
                            <option value="">Select</option>
                            @foreach($response as $key => $val)
                                <option value="{{$val->id}}">{{$val->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Best Practice Area</label>
                        <input type="text" class="form-control" name="practiceArea" placeholder="Best Practice Area"
                               v-model="singlePracticeObj.name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Best Practice Area</label>
                        <textarea type="text" class="form-control" name="practiceArea" placeholder="Recommendation"
                                  v-model="singlePracticeObj.recommendation"></textarea>
                    </div>
                    <button class="pull-right btn btn-info" type="button" @click="editSinglePracticeArea">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
