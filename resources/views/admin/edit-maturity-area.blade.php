@extends('admin.app')

@section('content')
    <div class="d-flex flex-column bd-highlight mb-3">
        <div class="col-md-12 p-0">
            <h4 class="mt-3">Edit @{{ singleMaturityObj.maturity }} Details
                <a class="pull-right ml-2 link" style="font-size: 17px;" href="{{ url('admin/maturity-areas') }}">
                    <i class="material-icons">undo</i>Back
                </a>
            </h4>
            <form>
                <div class="col-md-12 p-0 mt-2">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Name"
                               v-model="singleMaturityObj.name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Defination</label>
                        <textarea type="text" class="form-control" name="defination" placeholder="Defination"
                                  v-model="singleMaturityObj.definition"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Maturity Text</label>
                        <textarea type="text" class="form-control" name="maturityText" placeholder="Maturity Text"
                                  v-model="singleMaturityObj.text" v-validate="'required'"></textarea>
                    </div>
                    <button class="pull-right btn btn-info" type="button" @click="editSingleMaturity">Save
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
