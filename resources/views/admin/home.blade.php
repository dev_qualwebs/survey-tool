@extends('admin.app')

@section('content')
    <div class="col-md-12 p-0 user-count">
        <div class="col-md-12">
            <h2 style="padding-top: 18px;">Total User: @{{dashboardDetails.userCount}}</h2>
        </div>
    </div>
    <div class="col-md-12 p-0">
        <h4>Maturity Area Score</h4>
        <canvas id="maturityAdmin"></canvas>
    </div>
    <div class="col-md-12 p-0">
        <h4>Best Practice Area Score</h4>
        <canvas id="bestPracticeAdmin" height="500px"></canvas>
    </div>
@endsection
