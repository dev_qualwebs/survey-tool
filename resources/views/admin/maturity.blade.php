@extends('admin.app')

@section('content')
        <div class="col-md-12 p-0">
            <h4 class="mt-3">Maturity Areas
                <a class="pull-right btn btn-info" href="{{ url('admin/add-maturity-area') }}">Add</a>
            </h4>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 p-0">
            <div class="d-flex flex-column bd-highlight mb-3 p-0">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <th>#</th>
                        <th>Maturity Area</th>
                        <th>Definition</th>
                        <th>Maturity Text</th>
                        </thead>
                        <tbody>
                        @foreach($response as $key => $val)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td><a class="link"
                                       href="{{ url('admin/edit-maturity-area/'.$val->id) }}">{{ $val->name }}</a></td>
                                <td>{{ $val->definition }}</td>
                                <td>{{ $val->text }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection
