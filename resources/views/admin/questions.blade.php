@extends('admin.app')

@section('content')
    <div class="col-md-12 p-0">
        <h4 class="mt-3">Questions
            <a class="pull-right btn btn-info" href="{{ url('admin/add-question') }}">Add</a>
        </h4>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 p-0">
        <div class="d-flex flex-column bd-highlight mb-3">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <th>#</th>
                    <th>Maturity Area</th>
                    <th>Best Practice Area</th>
                    <th>Question</th>
                    </thead>
                    <tbody>
                    @foreach($response as $key => $val)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $val->maturity_area_name }}</td>
                            <td>{{ $val->best_practice_area_name }}</td>
                            <td><a class="link" href="{{ url('admin/single-question/'.$val->id) }}">{{ $val->question }}</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
