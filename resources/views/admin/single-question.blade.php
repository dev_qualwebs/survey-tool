@extends('admin.app')

@section('content')
    <div class="d-flex flex-column bd-highlight mb-3">
        <div class="col-md-12 p-0" id="viewFrom">
            <div class="col-md-12 p-0">
                <div class="row">
                    <div class="col-md-10 mt-2">
                        <h5 class="link"><b>Question</b> - @{{ ques.question }}</h5>
                    </div>
                    <div class="col-md-2 mt-2">
                        <button class="pull-right btn btn-info m-2" type="button" @click="checkEdit('show')">Edit
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-12 p-0" v-for="op in questionsDetails">
                <p><b><span class="link">Option</span> @{{ op.option }}</b> @{{ op.text }}</p>
                <p><b> Result</b> - @{{ op.result_text }}</p>
            </div>
        </div>
        <div class="col-md-12 p-0 mt-2" id="editForm" style="display: none;">
            <h4>Edit Question Details</h4>
            <form>
                <div class="col-md-12 p-0">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Question</label>
                        <input type="text" class="form-control" name="question" placeholder="Question"
                               v-model="questionsUpdateObj.question">
                    </div>
                    <div v-for="ans in questionsUpdateObj.answers">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Option @{{ ans.option }}</label>
                            <input type="text" class="form-control" :placeholder="'Option' + ans.option"
                                   v-model="ans.text">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Result for option @{{ ans.option }}</label>
                            <input type="text" class="form-control" :placeholder="'Result' + ans.option"
                                   v-model="ans.result_text">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 p-0">
                    <button class="pull-right btn btn-info m-2" type="button" @click="checkEdit('hide')">Cancel</button>
                    <button class="pull-right btn btn-info m-2" type="button" @click="editQuestionDetails">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
