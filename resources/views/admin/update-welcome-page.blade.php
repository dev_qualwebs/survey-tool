@extends('admin.app')

@section('content')
    <div class="d-flex flex-column bd-highlight mb-3">
        <form>
            <div class="form-group mt-3">
                <textarea class="form-control text-wel" v-model="welcomecontent.heading_1"></textarea>
            </div>
            <div class="form-group mt-3">
                <textarea class="form-control text-wel" v-model="welcomecontent.heading_2"></textarea>
            </div>
            <div class="form-group mt-3  text-center">
                <div id="fileupload">
                    <div class="image-dropzone">
                        <div class="box-image-main">
                            <label class="fileContainer button">
                                <i class="fa fa-camera-retro" title="Upload Image"></i>
                                <input type="file" id="image" style="width: 50px;height: 25px;" @change="uploadImg"/>
                            </label>
                        </div>
                    </div>
                </div>
                <img class="img-fluid" style="height: 300px;" :src="welcomecontent.image">
            </div>
            <div class="form-group mt-3">
                <editor v-model="welcomecontent.bottom_content"></editor>
            </div>
            <div class="form-group mt-3">
                <h5><b>Links</b></h5>
                <input class="form-control input-wel" v-model="welcomecontent.home_page_link">
            </div>
            <div class="form-group mt-3">
                <input class="form-control input-wel" v-model="welcomecontent.sports_analytics_link">
            </div>
            <div class="form-group mt-3">
                <input class="form-control input-wel" v-model="welcomecontent.attention_coaches_link">
            </div>
            <div class="form-group mt-3">
                <input class="form-control input-wel" v-model="welcomecontent.elit_athlete_link">
            </div>
            <div class="form-group mt-3">
                <input class="form-control input-wel" v-model="welcomecontent.contact_us_link">
            </div>
            <div class="form-control">
                <button class="btn btn-info pull-right mt-3" type="button" @click="updateWelcomeContent">Submit</button>
            </div>
        </form>
    </div>
@endsection
