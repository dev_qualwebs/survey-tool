@extends('admin.app')

@section('content')
    <div class="col-md-12 p-0">
        <div class="d-flex flex-column bd-highlight mb-3 pt-2 p-0">
            <h4>User Details</h4>
            <div class="col-md-12 pl-0">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td class="td-wd"><b>Name</b></td>
                            <td><b>:</b> @{{ userdetailObj.user_name }}</td>
                        </tr>
                        <tr>
                            <td class="td-wd"><b>Email</b></td>
                            <td><b>:</b> @{{ userdetailObj.user_email }}</td>
                        </tr>
                        <tr>
                            <td class="td-wd"><b>Organization</b></td>
                            <td><b>:</b> @{{ userdetailObj.organization }}</td>
                        </tr>
                        <tr>
                            <td class="td-wd"><b>Year</b></td>
                            <td><b>:</b> @{{ userdetailObj.start_year }}</td>
                        </tr>
                        <tr>
                            <td class="td-wd"><b>Sports</b></td>
                            <td><b>:</b> @{{ userdetailObj.sports }}</td>
                        </tr>
                        <tr>
                            <td class="td-wd"><b>Competition Level</b></td>
                            <td><b>:</b> @{{ userdetailObj.competition_levels }}</td>
                        </tr>
                        <tr>
                            <td class="td-wd"><b>User Company Role</b></td>
                            <td><b>:</b> @{{ userdetailObj.user_roles_company | userRoles }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-12 p-0">
                <h4>Maturity Score</h4>
            </div>
            <div class="col-md-12 p-0">
                <div class="col-md-12 p-0">
                   <h5 class="link"> <b>Score : @{{ userMaturityScore.score }}%gi</b></h5>
                </div>
                <div class="col-md-12 p-0" style="margin-bottom: 10px">
                    <h6>
                        Your team-assessment results can be used to spark the conversation with your peers
                        and team
                        member,
                        or engage with Victor Holman to help you address weaknesses and help and execute you
                        sports
                        analytic
                        roadmap. “Maturity” describes how deeply and effectively your team aligns people,
                        processes,
                        technology and data to your analytics program to gain a competitive edge.
                    </h6>
                    <ul style="padding: 8px;">
                        <li>
                            @if(\Illuminate\Support\Facades\Auth::user())
                                <h5>
                                    Your data has been added to the global benchmark and your assessment has
                                    been sent
                                    to <b class="link"> @{{ userdetailObj.user_email }}</b>
                                </h5>
                            @endif
                        </li>
                        <li>
                            <h5>
                                Your overall sports analytics maturity is at the <b class="link">@{{ userMaturityScore.data.level }}</b>
                            </h5>
                        </li>
                        <li>
                            <h5>@{{ userMaturityScore.data.result }}</h5>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
