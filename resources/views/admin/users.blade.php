@extends('admin.app')

@section('content')
    <div class="col-md-12 p-0">
        <h4 class="mt-3">Users</h4>
        <div class="d-flex flex-column bd-highlight mb-3">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach($response as $key => $val)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $val->name }}</td>
                            <td>{{ $val->email }}</td>
                            <td><a class="link" href="{{ url('admin/user-detail/'.$val->user_id) }}">View</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
