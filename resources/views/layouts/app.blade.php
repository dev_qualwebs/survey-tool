<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.header')
<body >
<div class="loader" id="loaderdiv"
     style=" background-image: url('{{ asset('public/images/loading_spinner.gif')}}')"></div>
<div id="app">
    <div class="container-fluid">
        <div id="main" class="row">
            <div class="col-md-12 main">
                <div class="row">
                    <div class="col-md-3 left-content">
                        <div class="sticky">
                            <div class="col-md-12 top-logo text-center">
                                <a :href="welcomecontent.home_page_link">
                                    <img width="200px" src="{{ asset('public/images/main-log-site.png') }}">
                                </a>
                            </div>
                            <div class="col-md-12 side-links">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                     aria-orientation="vertical">
                                    @guest
                                        <a class="nav-link active" id="v-pills-welcome-tab"
                                           data-target="#v-pills-welcome"
                                           role="tab" aria-controls="v-pills-welcome" aria-selected="false">
                                            @else
                                                <a class="nav-link" id="v-pills-welcome-tab"
                                                   data-target="#v-pills-welcome" welcome="yes"
                                                   role="tab" aria-controls="v-pills-welcome" aria-selected="false">
                                                    @endguest
                                                    <span data-toggle="tooltip"
                                                          data-placement="right"
                                                          :title="tooltipText">
                                                     <span class="count active">1</span>
                                                        <span class="tab-link">Welcome</span>
                                                    </span>
                                                </a>
                                                @guest
                                                    <a class="nav-link" id="v-pills-team-profile-tab"
                                                       data-target="#v-pills-team-profile"
                                                       role="tab" aria-controls="v-pills-team-profile"
                                                       aria-selected="true">
                                                        @else
                                                            <a class="nav-link active" id="v-pills-team-profile-tab"
                                                               data-target="#v-pills-team-profile"
                                                               role="tab" aria-controls="v-pills-team-profile"
                                                               aria-selected="true">
                                                                @endguest
                                                                <span data-toggle="tooltip"
                                                                      data-placement="right"
                                                                      :title="tooltipText">
                                                                    <span class="count">2</span>
                                                                    <span class="tab-link">Team Profile</span>
                                                                </span></a>
                                                            <a class="nav-link" id="v-pills-data-tab"
                                                               data-target="#v-pills-data"
                                                               role="tab" aria-controls="v-pills-data"
                                                               aria-selected="false">
                                                                <span data-toggle="tooltip"
                                                                      data-placement="right"
                                                                      :title="tooltipText">
                                                                    <span class="count">3</span>
                                                                        <span class="tab-link">Data</span>
                                                                </span>
                                                            </a>
                                                            <a class="nav-link" id="v-pills-analytics-tab"
                                                               data-target="#v-pills-analytics"
                                                               role="tab" aria-controls="v-pills-analytics"
                                                               aria-selected="false">
                                                                <span data-toggle="tooltip"
                                                                      data-placement="right"
                                                                      :title="tooltipText">
                                                                <span class="count">4</span>
                                                                    <span class="tab-link">Analytics</span>
                                                                </span>
                                                            </a>
                                                            <a class="nav-link" id="v-pills-people-tab"
                                                               data-target="#v-pills-people"
                                                               role="tab" aria-controls="v-pills-people"
                                                               aria-selected="false">
                                                                <span data-toggle="tooltip"
                                                                      data-placement="right"
                                                                      :title="tooltipText">
                                                                <span class="count">5</span>
                                                                    <span class="tab-link">People</span>
                                                                </span>
                                                            </a>
                                                            <a class="nav-link" id="v-pills-process-tab"
                                                               data-target="#v-pills-process"
                                                               role="tab" aria-controls="v-pills-process"
                                                               aria-selected="false">
                                                                <span data-toggle="tooltip"
                                                                      data-placement="right"
                                                                      :title="tooltipText">
                                                                <span class="count">6</span>
                                                                    <span class="tab-link">Process</span>
                                                                </span>
                                                            </a>
                                                            <a class="nav-link" id="v-pills-technology-tab"
                                                               data-target="#v-pills-technology"
                                                               role="tab" aria-controls="v-pills-technology"
                                                               aria-selected="false">
                                                                <span data-toggle="tooltip"
                                                                      data-placement="right"
                                                                      :title="tooltipText">
                                                                <span class="count">7</span>
                                                                    <span class="tab-link">Technology</span>
                                                                </span>
                                                            </a>
                                                            <a class="nav-link" id="v-pills-strategy-tab"
                                                               data-target="#v-pills-Strategy"
                                                               role="tab" aria-controls="v-pills-strategy"
                                                               aria-selected="false">
                                                                <span data-toggle="tooltip"
                                                                      data-placement="right"
                                                                      :title="tooltipText">
                                                                    <span class="count">8</span>
                                                                    <span class="tab-link">Strategy</span>
                                                                </span>
                                                            </a>
                                                            <a class="nav-link" id="v-pills-profatibility-tab"
                                                               data-target="#v-pills-profatibility"
                                                               role="tab" aria-controls="v-pills-profatibility"
                                                               aria-selected="false">
                                                                <span data-toggle="tooltip"
                                                                      data-placement="right"
                                                                      :title="tooltipText">
                                                                 <span class="count">9</span>
                                                                    <span class="tab-link">Profitability</span>
                                                                </span>
                                                            </a>
                                                            <a class="nav-link" id="v-pills-view-assess-tab"
                                                               assesment="yes"
                                                               data-target="#v-pills-view-assess"
                                                               role="tab" aria-controls="v-pills-view-assess"
                                                               data-target="#v-pills-view-assess"
                                                               aria-selected="false">
                                                                <span data-toggle="tooltip"
                                                                      data-placement="right"
                                                                      :title="tooltipText">
                                                                 <span class="count">10</span>
                                                                    <span class="tab-link">View Assessment</span>
                                                                </span>
                                                            </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 right-content p-0">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                                    <li class="nav-item"><a class="nav-link" target="_blank" :href="welcomecontent.sports_analytics_link">Sports Analytics Methods<span class="sr-only">(current)</span></a></li>
                                    <li class="nav-item"><a class="nav-link" target="_blank" :href="welcomecontent.attention_coaches_link">Attention Coaches</a></li>
                                    <li class="nav-item"><a class="nav-link" target="_blank" :href="welcomecontent.elit_athlete_link">Elite Athlete Counseling</a></li>
                                    <li class="nav-item"><a class="nav-link" target="_blank" :href="welcomecontent.contact_us_link">Contact Us</a></li>
                                    @guest
                                        <li class="nav-item active">
                                            <a class="nav-link" data-target="#">Assesment</a>
                                        </li>
                                    @else
                                        <li class="nav-item dropdown">
                                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#"
                                               role="button"
                                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right"
                                                 aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                      style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </li>
                                    @endguest
                                </ul>
                            </div>
                        </nav>
                        @guest
                            <div class="col-md-12 p-0 tab-view">
                                @yield('content')
                            </div>
                        @else
                            <div class="col-md-12 p-0 tab-view">
                                @yield('section')
                            </div>
                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="row">
    @include('layouts.footer')
</footer>
</body>
</html>
