<script type="text/javascript" src="https://platform.linkedin.com/in.js">
    api_key: 81gyo0fnfq140l
    authorize: false
</script>
<script type="text/javascript" src="{{ asset('node_modules/jquery/dist/jquery.js')}}"></script>
<script type="text/javascript">
    function linkdinlog() {
        IN.UI.Authorize().place();
        IN.Event.on(IN, "auth", onLinkedInAuth);
    }

    /*------function 1st that called after click on login with linked and authorise the user---------*/
    /*function onLinkedInLoad() {
        IN.Event.on(IN, "auth", onLinkedInAuth);
    }*/

    /*-------funciton 2nd that authanticted user and get user's data--------------------------------*/
    function onLinkedInAuth() {
        IN.API.Profile("me").fields("first-name", "last-name", "email-address", "positions", "id").result(function (data) {
            document.getElementById('loaderdiv').style.display = 'block';
            $.ajax({
                type: "POST",
                url: 'api/linkedinlogin',
                data: JSON.stringify(data.values[0]),
                dataType: 'JSON',
                contentType: 'application/json',
                success: function (res) {
                    document.getElementById('linkedUserRegisteredId').value = res.id;
                    document.getElementById('linkedin-form').submit();
                },
                error: function (res) {
                    document.getElementById('loaderdiv').style.display = 'none';
                    console.log(res);
                }
            });
        }).error(function (data) {
            document.getElementById('loaderdiv').style.display = 'none';
            console.log(data);
        });
        IN.User.logout();
    }
</script>
<script type="text/javascript" src="{{ asset('node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.js')}}"></script>
<script type="text/javascript"
        src="{{ asset('node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('node_modules/sweetalert2/dist/sweetalert2.js') }}"></script>
<script type="text/javascript" src="{{ asset('node_modules/daemonite-material/js/material.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/vue/dist/vue.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/chart.js/dist/Chart.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/vee-validate/dist/vee-validate.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/axios/dist/axios.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/select2/dist/js/select2.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/highcharts/highcharts.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/highcharts/highcharts-more.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/highcharts/modules/exporting.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/highcharts/modules/export-data.js')}}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/js/custom.js')}}"></script>
<script type="text/javascript" src="{{ asset('resources/assets/js/charts.js')}}"></script>

