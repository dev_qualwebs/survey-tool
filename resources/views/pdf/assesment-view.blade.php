<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.header')
<body>
<div id="app" style="background-color: #fff">
    <div class="col-md-12">
        <div class="col-md-12 heading-sec">
             <img src="{{ asset('public/images/agile.png') }}">
             <h1>Team Name : {{ $team_name['organization'] }}</h1>
             <h2 style="color: #2196f3">Overall Maturity Score</h2>
             <p><b>Score: {{ $overall_score }} %</b></p>
             <p>Your team-assessment results can be used to spark the conversation with your peers and team member,
                 or engage with Victor Holman to help you address weaknesses and help and execute you sports analytic
                 roadmap. “Maturity” describes how deeply and effectively your team aligns people, processes,
                 technology and data to your analytics program to gain a competitive edge.</p>
             <ul>
                 <li>@if(\Illuminate\Support\Facades\Auth::user())<p>Your data has been added to the global benchmark
                         and your assessment has been sent to
                         <b>{{\Illuminate\Support\Facades\Auth::user()->email}}</b></p>@endif</li>
                 <li><p>Your overall sports analytics maturity is at the <b>{{ $data['text'] }}</b></p></li>
                 <li><p>{{ $data['result'] }}</p></li>
             </ul>
             <hr>
             <h2 style=" color: #2196f3">Maturity Area Scores</h2>
             @foreach($Maturity_area as $key => $val)
                 <h3>{{ $key+1 }} {{$val->maturity}}</h3>
                 <p><b>Score: {{ $val->average }} %</b></p>
                 <p>{{ $val->definition }}</p>
                 <p>Your overall sports analytics maturity is at the <b>{{ $val->data['text'] }}</b></p>
                 <p>{{ $val->data['result'] }}</p>
                 <p>Steps you can take to improve your maturity score:</p>
                 @foreach($val->steps as $k => $v)
                     <ul>
                         <li v-for="step in area.steps"><p>{{ $v->result_text }}</p></li>
                     </ul>
                 @endforeach
                 <p>{{ $val->maturity_text }}</p>
             @endforeach
            <hr>
            <h2 style=" color: #2196f3">Best Practice Area Scores</h2>
            <div class="table-responsive">
                <table cellspacing="0" cellpadding="6" >
                    <tbody>
                    <tr>
                        <th style="border: 1px solid #d1d1d1"><p><b>Best Practice Area</b></p></th>
                        <th style="width: 50px;margin-bottom:50px;border: 1px solid #d1d1d1"><p><b>Score</b></p></th>
                        <th style="width: 300px;margin-bottom:50px;border: 1px solid #d1d1d1"><p><b>Recommendation</b></p></th>
                    </tr>
                    @foreach($practice_area as $k => $v)
                        <tr>
                            <td style="border: 1px solid #d1d1d1"><p>{{ $v->best_practice_area }}</p></td>
                            <td style="width: 50px;margin-bottom:50px;border: 1px solid #d1d1d1"><p>{{ $v->average }} %</p></td>
                            <td style="width: 300px;margin-bottom:50px;border: 1px solid #d1d1d1"><p>{{ $v->practice_recommendation }}</p></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr>
            <h2 style=" color: #2196f3">QUESTIONS ABOUT YOUR RESULTS?</h2>
            <p>Contact Victor Holman at victor@agilesportsanalytics.com, or call (888) 861-8733.</p>
            <p><b>Agile Sports Analytics can help you:</b></p>
            <ul>
                <li><p>Define your team identity, goals and team strategy</p></li>
                <li><p>Analyze data, define value and build player profiles</p></li>
                <li><p>Create a seamless flow of data across scouting, cap management, player performance and player
                        information</p></li>
                <li><p>Build decision management systems with real time and historical data</p></li>
                <li><p>dentify metrics that maximize player and team performance</p></li>
                <li><p>Build predictive models for acquiring prospects, recruits and trades</p></li>
                <li><p>Design automated workflows and secure their data</p></li>
                <li><p>Implement a clear process for executing their game plan</p></li>
                <li><p>Gain a competitive edge leveraging your existing analytic program</p></li>
            </ul>
            <p><b>The Agile Sports Analytics Application can help your team:</b></p>
            <ul>
                <li><p>Apply the roles, tools, events and proven processes to increase player IQ and create team
                        synergy.</p></li>
                <li><p>Measure player value, translate complex metrics into player results</p></li>
                <li><p>Demonstrate that they have the ability to adapt and create value for a team.</p></li>
                <li><p>Reward players by the value they produce to reach team goals</p></li>
                <li><p>Find players who fit a specific value profile based on your teams needs</p></li>
            </ul>
        </div>
    </div>
</div>
{{--<footer class="row">
    @include('layouts.footer')
</footer>--}}
</body>
</html>

