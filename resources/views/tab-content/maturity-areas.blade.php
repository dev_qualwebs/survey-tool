<div class="container-fluid container" v-if="maturityQues[0]">
    <div class="col-md-12 main tab-views">
        <div class="row">
            @if(count($next))
                <form @submit.prevent="validateForm('{{$next[0]}}','{{$next[1]}}','{{$next[2]}}')">
                    @endif
                    <p class="control">
                    <div class="col-md-12 questions-main">
                        <div v-for="(questionsArray, index) in maturityQues">
                            <div v-for="(ques, key) in questionsArray.questions" class="pt-3">
                                <h4 class="question"> @{{ ques.question }}</h4>
                                <div class="options">
                                    <div class="custom-control custom-radio" v-for="option in ques.options">
                                        <input :id="option.question_id+option.option" :name="'option'+ques.id"
                                               type="radio" :value="option.id" v-model="setMaturityArea[ques.id]"
                                               v-on:change="updateMaturityAnswer(ques.id, option.id)"
                                               class="custom-control-input"
                                               v-validate="'required'">
                                        <label class="custom-control-label" :for="option.question_id+option.option">
                                            @{{ option.text }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </p>
                    <p class="control">
                    @if(count($prev))
                        <div class="col-md-12 margin-bottom-10 option-control">
                            <div class="row">
                                <div class="col-md-6 col-6" align="left">
                                    <button class="nav-link btn btn-info" type="button"
                                            v-on:click="changeTab('{{$prev[0]}}', '{{$prev[1]}}', '{{$prev[2]}}')">
                                        <i class="fa fa-backward" aria-hidden="true"></i> Previous
                                    </button>
                                </div>
                                <div class="col-md-6 col-6" align="right">
                                    <button class="nav-link btn btn-info" type="submit" name="button">
                                        Next <i class="fa fa-forward" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        @endif
                        </p>
                </form>
        </div>
    </div>
</div>

