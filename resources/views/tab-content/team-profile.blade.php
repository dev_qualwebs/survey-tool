<div id="teamPage" class="container-fluid container">
    <div class=" col-md-12 main tab-views">
        <form class="row">
            <div class="col-md-12 p-0">
                <div class="loged-in-area">
                    @guest
                    @else
                        <p class="alert-success">
                            Signed in as <span>{{ Auth::user()->name }}</span>
                        </p>
                        <div class="col-md-12 padding-5">
                            <label class="label-color">Your Email:</label>
                            <input type="text" class="form-control custom-control" class="" placeholder="Email"
                                   value="{{ Auth::user()->email }}">
                        </div>
                    @endguest
                    <div class="col-md-12 padding-5">
                        <label class="label-color">Pick the organization to assess:</label>
                        <select class="form-control" v-model="teamProfileObj.organization">
                            <option value="">Select</option>
                            <option  v-for="(pos, index) in positions">
                                @{{ pos.company_name }}
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12 padding-0">
                <form>
                    <div class="col-md-12 margin-top-10">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <h5 class="question">1. What year did you start working with the team?</h5>
                                    <select class="form-control" v-model="teamProfileObj.start_year">
                                        <option value="">Select</option>
                                        <option v-for="y in years" :value="y.year">
                                            @{{ y.year }}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <h5 class="question">2. Please select the sport(s) your team plays?</h5>
                                    <select2-multiple id="select-sports" :options="sports_data"
                                                      v-model="teamProfileObj.sports_data"></select2-multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 margin-top-10">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <h5 class="question">3. Please select the competition level your team(s) plays?</h5>
                                    {{-- <select2l-level id="select-level" :options="competition_levels_data"
                                                     v-model="teamProfileObj.competition_levels_data">
                                         <option disabled value="0">Select</option>
                                     </select2l-level>--}}
                                    <select class="form-control" v-model="teamProfileObj.competition_levels">
                                        <option value="">Select</option>
                                        <option v-for="comp in competition_levels_data" :value="comp.name">
                                            @{{ comp.name }}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <h5 class="question">4. Please select the answer that best describes your role at your
                                        team/organization.</h5>
                                    <div class="col-md-12 custom-control p-0">
                                        {{-- <input class="custom-control-input" name="role.role" type="radio" :id="role.role"
                                                :value="role.id"
                                                v-model="teamProfileObj.user_roles_company">
                                         <label class="custom-control-label" :for="role.role">@{{ role.role }}</label>--}}
                                        <select class="form-control" v-model="teamProfileObj.user_roles_company">
                                            <option value="">Select</option>
                                            <option v-for="(role, index) in user_roles_company" :value="role.id">
                                                @{{ role.role }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 margin-top-10">
                        <div class="form-group">
                            <h5 class="question">5. Which of these challenges does your analytics program currently
                                face?
                                (Choose All That
                                Apply):</h5>
                            <div class="col-md-12 custom-control custom-checkbox "
                                 v-for="(ch, index) in challenges">
                                <input class="custom-control-input" type="checkbox" :id="index+1" :value="ch.id"
                                       v-model="teamProfileObj.challenges">
                                <label class="custom-control-label" :for="index+1">@{{ ch.challenge }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 margin-top-10">
                        <div class="form-group">
                            <h5 class="question">6. Which roles do you have on staff in your analytics program?
                                (Choose
                                All
                                That
                                Apply):</h5>
                            <div class="col-md-12 custom-control custom-checkbox "
                                 v-for="(role, index) in userRoleStaff">
                                <input class="custom-control-input" type="checkbox" :id="role.role" :value="role.id"
                                       v-model="teamProfileObj.user_roles_staff">
                                <label class="custom-control-label" :for="role.role">@{{ role.role }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 margin-top-10">
                        <div class="form-group">
                            <h5 class="question">
                                7. Which sports analytics method(s) do you apply to your data to optimize
                                performance?
                                <br>
                                (Choose All That Apply)
                            </h5>
                            <small>Drag from left to right</small>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5 drop-box">
                                        <p style="cursor: grab" draggable="true"
                                           v-for="(method, index) in analyticsMethods"
                                           :id="'dragtarget'+method.id" :method="method.id">@{{ method.name }}</p>
                                    </div>
                                    <div id="dropadatavalue" class="col-md-5 drop-box" style="margin-left: 25px;">
                                        <p style="cursor: grab" draggable="true"
                                           v-for="(method, index) in userTeamProfile.analyticMethods"
                                           :id="'dragtarget'+method.id" :method="method.id">@{{ method.name }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-md-12 margin-bottom-10 option-control">
                    <div class="row">
                        <div class="col-md-6 col-6" align="left">
                            <button type="button" class="nav-link btn btn-info"
                                    v-on:click="changeTab('v-pills-team-profile','v-pills-welcome')">
                                <i class="fa fa-backward" aria-hidden="true"></i> Previous
                            </button>
                        </div>
                        <div class="col-md-6 col-6" align="right">
                            <button class="nav-link btn btn-info" type="button"
                                    v-on:click="updateTeamProfile('v-pills-team-profile', 'v-pills-data', 'Data')">
                                Next <i class="fa fa-forward" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/x-template" id="select2-template">
    <select multiple>
        <slot></slot>
    </select>
</script>
<script type="text/x-template" id="select2-level">
    <select multiple>
        <slot></slot>
    </select>
</script>
