<div class="container-fluid">
    <div class="col-md-12 main">
        <div class="row">
            <div class="col-md-12 padding-0">
                <nav class="assess-navbar navbar-expand-lg navbar-light">
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <div class="nav nav-pills assess-tab-active " id="v-pills-tab" role="tablist"
                             aria-orientation="horizontal">
                            <a class="nav-link assess-link active" id="v-pills-overall-score-tab" data-toggle="pill"
                               data-target="#v-pills-overall-score"
                               role="tab" aria-controls="v-pills-overall-score" aria-selected="true"
                               @click="getOverAllScore">
                                Overall Score
                            </a>
                            <a class="nav-link assess-link" id="v-pills-maturity-score-tab" data-toggle="pill"
                               data-target="#v-pills-maturity-score"
                               role="tab" aria-controls="v-pills-maturity-score" aria-selected="false"
                               @click="getMaturiyAreaScore">
                                Maturity Category Scores
                            </a>
                            <a class="nav-link assess-link" id="v-pills-prcatices-score-tab" data-toggle="pill"
                               data-target="#v-pills-prcatices-score"
                               role="tab" aria-controls="v-pills-prcatices-score" aria-selected="false"
                               @click="getBestPracticesAreaScore">
                                Best Practices Scores
                            </a>
                            <a class="nav-link assess-link" id="v-pills-benchmark-score-tab" data-toggle="pill"
                               data-target="#v-pills-benchmark-score"
                               role="tab" aria-controls="v-pills-benchmark-score" aria-selected="false"
                               @click="getOtherTeamChallenges">
                                Benchmarks
                            </a>
                        </div>
                    </div>
                </nav>
                <div class="col-md-12 p-0 assess-tab-view">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-overall-score" role="tabpanel"
                             aria-labelledby="v-pills-overall-score-tab">
                            <div class="col-md-12 questions-main">
                                <h3>Overall Maturity Score</h3>
                                <div class="col-md-6">
                                    <div class="wrapper">
                                        <div class="rang">
                                            <div class="rang-title">
                                                <input class="rang-number" id="show" type="text"
                                                       :value="overAllScore+''+'%'"
                                                       disabled="disabled"/>
                                            </div>
                                            <svg class="meter">
                                                <circle class="meter-left" r="96" cx="135" cy="142"></circle>
                                                <circle class="meter-center" r="96" cx="136" cy="142"></circle>
                                                <circle class="meter-center-l" r="96" cx="134" cy="142"></circle>
                                                <circle class="meter-center-r" r="96" cx="134" cy="142"></circle>
                                                <circle class="meter-right" r="96" cx="138" cy="142"></circle>
                                                <polygon class="meter-clock" points="129,145 137,90 145,145"></polygon>
                                                <circle class="meter-circle" r="10" cx="137" cy="145"></circle>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom: 10px">
                                <h4>
                                    Your team-assessment results can be used to spark the conversation with your peers
                                    and team
                                    member,
                                    or engage with Victor Holman to help you address weaknesses and help and execute you
                                    sports
                                    analytic
                                    roadmap. “Maturity” describes how deeply and effectively your team aligns people,
                                    processes,
                                    technology and data to your analytics program to gain a competitive edge.
                                </h4>
                                <ul>
                                    <li>
                                        @if(\Illuminate\Support\Facades\Auth::user())
                                            <h4>
                                                Your data has been added to the global benchmark and your assessment has
                                                been sent
                                                to <b>{{\Illuminate\Support\Facades\Auth::user()->email}}</b>
                                            </h4>
                                        @endif
                                    </li>
                                    <li>
                                        <h4>
                                            Your overall sports analytics maturity is at the
                                            <b>@{{ overAllScoreData.text }}</b>
                                        </h4>
                                    </li>
                                    <li>
                                        <h4>@{{ overAllScoreData.result }}</h4>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-maturity-score" role="tabpanel"
                             aria-labelledby="v-pills-maturity-score-tab">
                            <div class="col-md-12 pt-4 pl-0" v-for="(area, index) in teamAnalyticScores">
                                <div class="col-md-12">
                                    <h3>@{{ index+1 }} @{{area.maturity}}</h3>
                                </div>
                                <div class="wrapper">
                                    <div class="rang">
                                        <div class="rang-title">
                                            <input class="rang-number" :id="area.maturity" type="text"
                                                   :value="area.average+''+'%'"
                                                   disabled="disabled"/>
                                        </div>
                                        <svg class="meter">
                                            <circle class="meter-left" r="96" cx="135" cy="142"></circle>
                                            <circle class="meter-center" r="96" cx="136" cy="142"></circle>
                                            <circle class="meter-center-l" r="96" cx="134" cy="142"></circle>
                                            <circle class="meter-center-r" r="96" cx="134" cy="142"></circle>
                                            <circle class="meter-right" r="96" cx="138" cy="142"></circle>
                                            <polygon class="meter-clock" points="129,145 137,90 145,145"></polygon>
                                            <circle class="meter-circle" r="10" cx="137" cy="145"></circle>
                                        </svg>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <h4> @{{ area.definition }}</h4>
                                    <h4> Your overall sports analytics maturity is at the <b>@{{ area.data.text }}</b>
                                    </h4>
                                    <h4> @{{ area.data.result }}</h4>
                                    <h4>Steps you can take to improve your maturity score:</h4>
                                    <ul>
                                        <li v-for="step in area.steps">
                                            <h4> @{{ step.result_text }} </h4>
                                        </li>
                                    </ul>
                                    <h4> @{{ area.maturity_text }}</h4>
                                </div>
                            </div>
                            <div class="col-md-12 pt-4">
                                <h3 style=" color: #2196f3">QUESTIONS ABOUT YOUR RESULTS?</h3>
                                <h4>Contact Victor Holman at victor@agilesportsanalytics.com, or call (888) 861-8733.</h4>
                                <h4><b>Agile Sports Analytics can help you:</b></h4>
                                <ul>
                                    <li><h4>Define your team identity, goals and team strategy</h4></li>
                                    <li><h4>Analyze data, define value and build player profiles</h4></li>
                                    <li><h4>Create a seamless flow of data across scouting, cap management, player performance and player
                                            information</h4></li>
                                    <li><h4>Build decision management systems with real time and historical data</h4></li>
                                    <li><h4>dentify metrics that maximize player and team performance</h4></li>
                                    <li><h4>Build predictive models for acquiring prospects, recruits and trades</h4></li>
                                    <li><h4>Design automated workflows and secure their data</h4></li>
                                    <li><h4>Implement a clear process for executing their game plan</h4></li>
                                    <li><h4>Gain a competitive edge leveraging your existing analytic program</h4></li>
                                </ul>
                                <h4><b>The Agile Sports Analytics Application can help your team:</b></h4>
                                <ul>
                                    <li><h4>Apply the roles, tools, events and proven processes to increase player IQ and create team
                                            synergy.</h4></li>
                                    <li><h4>Measure player value, translate complex metrics into player results</h4></li>
                                    <li><h4>Demonstrate that they have the ability to adapt and create value for a team.</h4></li>
                                    <li><h4>Reward players by the value they produce to reach team goals</h4></li>
                                    <li><h4>Find players who fit a specific value profile based on your teams needs</h4></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-prcatices-score" role="tabpanel"
                             aria-labelledby="v-pills-prcatices-score-tab">
                    {{--        v-if="!(area.best_practice_area == 'Data and Reporting' && area.maturity_area == 'Data')
                            && !(area.best_practice_area == 'Data and Reporting' && area.maturity_area == 'Strategy')"--}}
                            <div class="col-md-12 pt-4 pl-0" v-for="(area, index) in bestPracticeAreaScores">
                                <div class="col-md-12">
                                    <h3>@{{ index+1 }} @{{area.best_practice_area}}</h3>
                                </div>
                                <div class="wrapper">
                                    <div class="rang">
                                        <div class="rang-title">
                                            <input class="rang-number" :id="area.best_practice_area" type="text"
                                                   :value="area.average+''+'%'"
                                                   disabled="disabled"/>
                                        </div>
                                        <svg class="meter">
                                            <circle class="meter-left" r="96" cx="135" cy="142"></circle>
                                            <circle class="meter-center" r="96" cx="136" cy="142"></circle>
                                            <circle class="meter-center-l" r="96" cx="134" cy="142"></circle>
                                            <circle class="meter-center-r" r="96" cx="134" cy="142"></circle>
                                            <circle class="meter-right" r="96" cx="138" cy="142"></circle>
                                            <polygon class="meter-clock_1" points="129,145 137,90 145,145"></polygon>
                                            <circle class="meter-circle" r="10" cx="137" cy="145"></circle>
                                        </svg>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <h4> @{{ area.practice_recommendation }}</h4>
                                </div>
                            </div>
                           {{-- <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Score</th>
                                        <th>Recommendation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="area in bestPracticeAreaScores">
                                        <td>
                                            @{{ area.best_practice_area }}
                                        </td>
                                        <td>
                                            @{{ area.average }}%
                                        </td>
                                        <td>
                                            @{{ area.practice_recommendation }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>--}}
                        </div>
                        <div class="tab-pane fade" id="v-pills-benchmark-score" role="tabpanel"
                             aria-labelledby="v-pills-benchmark-score-tab">
                            <nav class="assess-navbar navbar-expand-lg navbar-light">
                                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                                    <div class="nav nav-pills assess-tab-active " id="v-pills-tab" role="tablist"
                                         aria-orientation="horizontal">
                                        <a class="nav-link assess-link active" id="v-pills-challenges-tab"
                                           data-toggle="pill"
                                           data-target="#v-pills-challenges"
                                           role="tab" aria-controls="v-pills-challenges" aria-selected="true"
                                           @click="getOtherTeamChallenges">
                                            Challenges</a>
                                        <a class="nav-link assess-link" id="v-pills-analtytic-chlng-tab"
                                           data-toggle="pill" data-target="#v-pills-analtytic-chlng"
                                           role="tab" aria-controls="v-pills-analtytic-chlng" aria-selected="false"
                                           @click="getOtherTeamAnalyticChallenges">
                                            Analytic Methods
                                        </a>
                                        <a class="nav-link assess-link" id="v-pills-other-analytic-tab"
                                           data-toggle="pill" data-target="#v-pills-other-analytic"
                                           role="tab" aria-controls="v-pills-other-analytic" aria-selected="false"
                                           @click="getMaturiyAreaScore">
                                            Maturity Areas
                                        </a>
                                        <a class="nav-link assess-link" id="v-pills-other-bestpractice-tab"
                                           data-toggle="pill" data-target="#v-pills-other-bestpractice"
                                           role="tab" aria-controls="v-pills-other-other-bestpractice"
                                           aria-selected="false"
                                           @click="getBestPracticesAreaScore">
                                            Best Practice Areas
                                        </a>
                                        <a class="nav-link assess-link" id="v-pills-other-roles-tab"
                                           data-toggle="pill" data-target="#v-pills-other-roles"
                                           role="tab" aria-controls="v-pills-other-other-roles"
                                           aria-selected="false" @click="getRolesBar">
                                            Roles
                                        </a>
                                    </div>
                                </div>
                            </nav>
                            <div class="col-md-12 p-0 assess-tab-view">
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-challenges" role="tabpanel"
                                         aria-labelledby="v-pills-challenges-tab">
                                        <div class="col-md-12">
                                            <h3>Your team’s analytics program faces the following challenges:</h3>
                                            <p v-for="ch in ourTeamChallenges">- @{{ ch.challenge }}</p>
                                        </div>
                                        <div class="col-md-12">
                                            <h4>Other teams in your competition level face the following
                                                challenges:</h4>
                                            <canvas id="otherTeamChlng"></canvas>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-analtytic-chlng" role="tabpanel"
                                         aria-labelledby="v-pills-analtytic-chlng-tab">
                                        <div class="col-md-12">
                                            <h3>Your team applies the following sports analytic methods to optimize
                                                performance:</h3>
                                            <p v-for="mth in ourTeamAnalyticMethods">- @{{ mth.name }}</p>
                                        </div>
                                        <div class="col-md-12 mt-4">
                                            <h4>Other teams in your competition level apply the following sports
                                                analytic methods:</h4>
                                            <canvas id="otherTeamAnaChlng"></canvas>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-other-analytic" role="tabpanel"
                                         aria-labelledby="v-pills-other-analytic-tab">
                                        <canvas id="maturityAreaComparison"></canvas>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-other-bestpractice" role="tabpanel"
                                         aria-labelledby="v-pills-other-bestpractice-tab">
                                        <canvas id="practiceAreaComparison" height="500px"></canvas>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-other-roles" role="tabpanel"
                                         aria-labelledby="v-pills-other-roles-tab">
                                        <div class="col-md-12">
                                            <h3>Your team’s analytics program has the following roles:</h3>
                                            <p v-for="mth in ourRolesScores">- @{{ mth.role }}</p>
                                        </div>
                                        <div class="col-md-12">
                                            <h4>Other teams in your competition level face employ the following
                                                analytic staff:</h4>
                                            <canvas id="rolesChallenges"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                </div>
            </div>
        </div>
    </div>
</div>
