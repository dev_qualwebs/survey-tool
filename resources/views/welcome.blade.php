@extends('layouts.app')

@section('section')
    <div class="tab-content" id="v-pills-tabContent">
        <div class="tab-pane fade" id="v-pills-welcome" role="tabpanel"
             aria-labelledby="v-pills-welcome-tab">
            <div class="col-md-12 content-inner" v-cloak>
                <div id="profiles"></div>
                <div class="heading-sec text-center">
                    <h2>@{{ welcomecontent.heading_1 }}</h2>
                    <h4>@{{ welcomecontent.heading_2 }}</h4>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center padding-0">
                        <img class="img-fluid" :src="welcomecontent.image" style="max-width: 75%;height: 100%">
                    </div>
                    <div class="col-md-12 padding-0 pt-1">
                        <div class="col-md-12">
                            @guest
                            @else
                                <div class="loged-user" style="width: 100%; margin: 0px;">
                                    <p class="alert-success">
                                        Signed in as <span>{{ Auth::user()->name }}</span>
                                    </p>
                                    <div class="col-md-12 padding-5">
                                        <label class="label-color">Your Email:</label>
                                        <input type="text" class="form-control" placeholder="Email"
                                               value="{{ Auth::user()->email }}">
                                    </div>
                                </div>
                            @endguest
                        </div>
                    </div>
                    <div v-html="welcomecontent.bottom_content"></div>
                </div>
            </div>
            <div class="col-md-12 margin-bottom-10" align="right">
                <button class="nav-link btn btn-info"
                        v-on:click="changeTab('v-pills-welcome', 'v-pills-team-profile');getTeamProfile();">
                    Next <i class="fa fa-forward" aria-hidden="true"></i>
                </button>
            </div>
        </div>
        <div class="tab-pane fade show active" id="v-pills-team-profile" role="tabpanel"
             aria-labelledby="v-pills-team-profile-tab">
            @include('tab-content.team-profile')
        </div>
        <div class="tab-pane fade" id="v-pills-data" role="tabpanel"
             aria-labelledby="v-pills-messages-tab">
            @include('tab-content.maturity-areas', ["prev" => ['v-pills-data', 'v-pills-team-profile', 'Off'], "next" => ['v-pills-data', 'v-pills-analytics', 'Analytics']])
        </div>
        <div class="tab-pane fade" id="v-pills-analytics" role="tabpanel"
             aria-labelledby="v-pills-settings-tab">
            @include('tab-content.maturity-areas', ["prev" => ['v-pills-analytics', 'v-pills-data', 'Data'], "next" => ['v-pills-analytics', 'v-pills-people', 'People']])
        </div>
        <div class="tab-pane fade" id="v-pills-people" role="tabpanel"
             aria-labelledby="v-pills-people-tab">
            @include('tab-content.maturity-areas', ["prev" => ['v-pills-people', 'v-pills-analytics', 'Analytics'], "next" => ['v-pills-people', 'v-pills-process', 'Process']])
        </div>
        <div class="tab-pane fade" id="v-pills-process" role="tabpanel"
             aria-labelledby="v-pills-process-tab">
            @include('tab-content.maturity-areas', ["prev" => ['v-pills-process', 'v-pills-people', 'People'], "next" => ['v-pills-process', 'v-pills-technology', 'Technology']])
        </div>
        <div class="tab-pane fade" id="v-pills-technology" role="tabpanel"
             aria-labelledby="v-pills-technology-tab">
            @include('tab-content.maturity-areas', ["prev" => ['v-pills-technology', 'v-pills-process', 'Process'], "next" => ['v-pills-technology', 'v-pills-strategy', 'Strategy']])
        </div>
        <div class="tab-pane fade" id="v-pills-strategy" role="tabpanel"
             aria-labelledby="v-pills-technology-tab">
            @include('tab-content.maturity-areas', ["prev" => ['v-pills-strategy', 'v-pills-technology', 'Technology'], "next" => ['v-pills-strategy', 'v-pills-profatibility', 'Profitability']])
        </div>
        <div class="tab-pane fade" id="v-pills-profatibility" role="tabpanel"
             aria-labelledby="v-pills-profatibility-tab">
            @include('tab-content.maturity-areas', ["prev" => ['v-pills-profatibility', 'v-pills-strategy', 'Strategy'], "next" => ['v-pills-profatibility', 'v-pills-view-assess', 'Off']])
        </div>
        <div class="tab-pane fade" id="v-pills-view-assess" role="tabpanel"
             aria-labelledby="v-pills-view-assess-tab">
            @include('tab-content.view-analysis', ["prev" => ['v-pills-view-assess', 'v-pills-profatibility', 'Profitability'], "next" => []])
            <div class="col-md-12 margin-bottom-10" align="left">
                <div class="row">
                    <div class="col-md-6">
                        <button class="nav-link btn btn-info"
                                v-on:click="changeTab('v-pills-view-assess', 'v-pills-profatibility', 'Profitability')">
                            <i class="fa fa-backward" aria-hidden="true"></i> Previous
                        </button>
                        <button class="nav-link btn btn-info" @click="generatePdf">Export
                            as PDF</button>
                        <a :href="filePath" download="" id="exportAssesmentPdf"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="col-md-12 content-inner" v-cloak>
        <div class="heading-sec text-center">
            <h2>@{{ welcomecontent.heading_1 }}</h2>
            <h4>@{{ welcomecontent.heading_2 }}</h4>
        </div>
        <div class="row">
            <div class="col-md-12 text-center padding-0">
                <img class="img-fluid" :src="welcomecontent.image" style="max-width: 75%;height: 100%">
            </div>
            <div class="col-md-12 padding-0 pt-3">
                <div class="col-md-12">
                    @guest
                        <div class="login-area" style="width: 100%; height: 110px; margin: 0px;">
                            <h5 style="font-weight: bold;">
                                Get started by signing in with
                            </h5>
                            <div class="col-md-12">
                                <div class="clearfix"></div>
                                <div class="row">
                                    {{--<a href="{{ url('/login/linkedin') }}" class="linkedin-btn">
                                        <i class="fa fa-linkedin fa-size" aria-hidden="true"></i>
                                        Sign in with Linkedin
                                    </a>--}}
                                    <a onclick="linkdinlog();" href="#" class="linkedin-btn">
                                        <i class="fa fa-linkedin fa-size" aria-hidden="true"></i>
                                        Sign in with Linkedin
                                    </a>
                                    <form id="linkedin-form" action="{{ url('user/login') }}" method="POST" hidden>
                                        @csrf
                                        <input type="hidden" id="linkedUserRegisteredId" name="registeredId">
                                    </form>
                                </div>
                                {{-- <strong style="padding: 7px;">OR</strong>
                                 <div class="row">
                                     <a href="{{ url('/login/facebook') }}" class="linkedin-btn">
                                         <i class="fa fa-facebook-square" aria-hidden="true"></i>
                                         Sign in with Facebook
                                     </a>
                                 </div>--}}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    @else
                    @endguest
                </div>
            </div>
            <div v-html="welcomecontent.bottom_content"></div>
        </div>
    </div>
@endsection
