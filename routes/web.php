<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/team-profile', function () {
    return view('team-profile');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('welcome');

Route::post('/user/login', 'Auth\LoginController@userLogin');

Route::get('/login/linkedin', 'Auth\LoginController@redirectToLinkedin');

Route::get('/login/linkedin/callback', 'Auth\LoginController@handleLinkedinCallback');

Route::get('/login/facebook', 'Auth\LoginController@redirectToFacebook');

Route::get('/login/facebook/callback', 'Auth\LoginController@handleFacebookCallback');

Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/assesment-pdf', 'HomeController@assesmentView');

/*
|--------------------------------------------------------------------------
| User Web APIs
|--------------------------------------------------------------------------
|
| APIs related to users to update team profile
|
|
 */

Route::group(['middleware' => ['auth'], 'prefix' => 'api/users'], function () {

    Route::get('get-team-profile', 'UserController@getTeamProfile');

    Route::post('update-team-profile', 'UserController@updateTeamProfile');

    Route::post('update-maturity-answer', 'UserController@updateMaturityAnswers');

    Route::get('overall-maturity-score', 'UserController@overallMaturityScore');

    Route::get('maturity-area-score', 'UserController@maturityAreaScore');

    Route::get('best-practice-area-score', 'UserController@bestPracticeAreaScore');

    Route::get('other-team-challenges', 'UserController@otherTeamsChallenges');

    Route::get('other-team-analytic-methods', 'UserController@otherTeamsAnalyticMethods');

    Route::get('other-team-maturity-score', 'UserController@overAllMaturityAreaScore');

    Route::get('other-best-practice-area-score', 'UserController@overAllBestPracticeAreaScore');

    Route::get('other-users-roles', 'UserController@otherUsersRoles');

    Route::get('get-user-positions', 'UserController@getUserPositions');

    Route::get('generate-pdf', 'UserController@generatePdf');
});

Route::group(['prefix' => 'api/common'], function () {

    Route::get('roles', 'DataController@getRoles');

    Route::get('challenges', 'DataController@getChallenges');

    Route::get('get-maturity-areas', 'DataController@getMaturityAreas');

    Route::get('get-maturity-questions', 'DataController@getMaturityQuestions');

    Route::get('get-analytic-methods', 'DataController@getAnalyticMethods');

    Route::get('get-welcome-content', 'UserController@getPageWelcomeContent');

});


/*
|--------------------------------------------------------------------------
| Admin Web APIs and Routes
|--------------------------------------------------------------------------
|
 */


Route::group(['middleware' => ['admin'], 'prefix' => 'api/admin'], function () {

    Route::post('edit-question', 'AdminController@editQuestion');

    Route::post('add-practice-area', 'AdminController@addPracticeArea');

    Route::post('post-maturity-area', 'AdminController@addMaturityArea');

    Route::post('single-maturity-area', 'AdminController@singleMaturityArea');

    Route::post('edit-single-maturity', 'AdminController@editSingleMaturity');

    Route::post('single-best-practice-area', 'AdminController@singleBestPracticeArea');

    Route::post('edit-single-best-practice-area', 'AdminController@editSinglePracticeArea');

    Route::post('get-user-profile', 'AdminController@getUserProfile');

    Route::post('user-maturity-score', 'AdminController@userMaturityScore');

    Route::get('users-graph', 'AdminController@usersGraph');

    Route::get('welcome-content', 'AdminController@getWelcomeContent');

    Route::post('update-welcome-content', 'AdminController@updateWelcomeContent');

    Route::post('upload-image', 'AdminController@uploadImage');

});

Route::group(['middleware' => ['admin'], 'prefix' => '/admin'], function () {

    Route::get('questions', 'AdminController@loadQuestionsPage');

    Route::get('best-practices', 'AdminController@loadBestPracticesPage');

    Route::get('maturity-areas', 'AdminController@loadMaturityAreaPage');

    Route::get('single-question/{qid}', 'AdminController@loadSingleQuestion');

    Route::get('add-best-practice-areas', 'AdminController@loadBestPracticeArea');

    Route::get('add-maturity-area', 'AdminController@loadAddMaturityArea');

    Route::get('edit-maturity-area/{id}', 'AdminController@loadEditMaturityArea');

    Route::get('edit-best-practice-area/{id}', 'AdminController@loadEditBestPracticeArea');

    Route::get('welcome-content', 'AdminController@loadWelcomeContent');

    Route::get('users', 'AdminController@loadUsersPage');

    Route::get('user-detail/{user_id}', 'AdminController@loadUserDetails');

    Route::get('add-question', 'AdminController@addQuestion');

    Route::post('post-add-question', 'AdminController@postAddQuestion');

});

Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('/admin/login', 'AdminAuth\AuthController@showLoginForm');

Route::post('/admin/logout', 'AdminAuth\AuthController@logout')->name('admin/logout');

Route::get('api/admin/get-questions', 'AdminController@getQuestions');

Route::get('api/admin/get-question-options/{qid}', 'AdminController@getQuestionOptions');

Route::get('api/admin/get-maturity-area', 'AdminController@getMaturityArea');

Route::get('api/admin/get-bp-area/{mid}', 'AdminController@getBPArea');

Route::get('api/admin/get-all-users', 'AdminController@getAllUsers');

Route::get('admin/add-question', 'AdminController@addQuestion');

Route::post('admin/post-add-question', 'AdminController@postAddQuestion');
